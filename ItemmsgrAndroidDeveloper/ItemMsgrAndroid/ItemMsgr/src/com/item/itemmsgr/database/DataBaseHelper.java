package com.item.itemmsgr.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseHelper extends SQLiteOpenHelper {
	private static String DB_PATH = "/data/data/com.item.itemmsgr/databases/";
	private static final String DATABASE_NAME = "cliente_emm.sqlite";
	private static final int DATABASE_VERSION = 1;
	private boolean databaseExist;
	
	public Context context;
	static SQLiteDatabase sqliteDataBase;

	public DataBaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
	}

	// check if the database exists
	public void createDataBase() throws IOException {
		Log.e("DataBaseHelper", "createDataBase");

		//this.databaseExist = existingDataBase();
		if(!existingDataBase()) 
		{
			this.getWritableDatabase();
			copyDataBase();			
		}
	}// end if else dbExist
		// end createDataBase().

	public boolean existingDataBase() {
		//Log.e("DataBaseHelper", "existingDataBase");
		File databaseFile = new File(DB_PATH + DATABASE_NAME);
		
		Log.e("DataBaseHelper", "existingDataBase:"+databaseFile.exists());
		return databaseFile.exists();
	}

	private void copyDataBase() throws IOException {
		Log.e("DataBaseHelper", "copyDataBase");
		// Open your local db as the input stream
		InputStream myInput = context.getAssets().open(DATABASE_NAME);
		// Path to the just created empty db
		String outFileName = DB_PATH + DATABASE_NAME;
		// Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);
		// transfer bytes from the input file to the output file
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}
		// Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();
	}

	/*          
	private void copyDataBase() throws IOException {
		Log.e("DataBaseHelper", "copyDataBase2");
		// Path to the empty database.
	    String outFilename = DB_PATH + DATABASE_NAME;
	    // Open the empty database as the output stream.
	    OutputStream outputDatabase = new FileOutputStream(outFilename);
	    // Transfer bytes from the input file to the output file.
	    byte[] buffer = new byte[1024];

//	    for (int i = 0; i < sFiles.length; ++i) {
	        // Open the local database as the input stream.
	        InputStream is = context.getAssets().open(DATABASE_NAME);

	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            outputDatabase.write(buffer, 0, length);
	        }
	        // Closing stream.
	        is.close();
  //}

	    // Closing the streams.
	    outputDatabase.flush();
	    outputDatabase.close();
	    // Closing database.
	    close();        
	}*/
	
	/*
	
	 public void copyDataBase() throws IOException{
			Log.e("DataBaseHelper", "copyDataBase3");

	       InputStream in  = context.getAssets().open(DATABASE_NAME);
	       Log.e("sample", "Starting copying" );
	       String outputFileName = DB_PATH+DATABASE_NAME;
	       File databaseFile = new File( DB_PATH);
	        // check if databases folder exists, if not create one and its subfolders
	        if (!databaseFile.exists()){
	            databaseFile.mkdir();
	        }
	      
	       OutputStream out = new FileOutputStream(outputFileName);
	      
	       byte[] buffer = new byte[1024];
	       int length;
	      
	      
	       while ((length = in.read(buffer))>0){
	              out.write(buffer,0,length);
	       }
	       Log.e("sample", "Completed" );
	       out.flush();
	       out.close();
	       in.close();
	      
	    }*/
	
	
	
	/**
	 * This method opens the data base connection. First it create the path up
	 * till data base of the device. Then create connection with data base.
	 */
	public void openDataBase() throws SQLException {
		Log.e("DataBaseHelper", "openDataBase");
		// Open the database
		try {
			createDataBase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String myPath = DB_PATH + DATABASE_NAME;
		sqliteDataBase = SQLiteDatabase.openDatabase(myPath, null,
				SQLiteDatabase.OPEN_READWRITE);
	}

	/**
	 * This Method is used to close the data base connection.
	 */
	@Override
	public synchronized void close() {
		Log.e("DataBaseHelper", "close");
		if (sqliteDataBase != null)
			sqliteDataBase.close();
		super.close();
	}

	// declare methods to fetch data
	public Cursor getBasicCategoryDetails() {
		return sqliteDataBase.rawQuery("write the query",null);
	}

	/*public void  getAllContacts() {
        //List<Contact> contactList = new ArrayList<Contact>();
        // Select All Query
        String selectQuery = "SELECT  * FROM CONTACT";
        Log.i("Contact","SELECT  * FROM CONTACT");  
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String data=""; 
                data+=":"+cursor.getString(0)+":";
                data+=":"+cursor.getString(1)+":";
                data+=":"+cursor.getString(2)+":";
                data+=":"+cursor.getString(3)+":";
                data+=":"+cursor.getString(4)+":";
                data+=":"+cursor.getString(5)+":";
                data+=":"+cursor.getString(6)+":";
                data+=":"+cursor.getString(7)+":";
              Log.i("Contact",data);  
            } while (cursor.moveToNext());
        }
        // return contact list
        //return contactList;
    }
 *//*
	
	public  SQLiteDatabase getDatabase(String type){	
		return this.getWritableDatabase();
	}
	*/
	
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
	}
	
	
	public static void main(String args[]){
		System.out.println("DB init");

		DataBaseHelper help=new DataBaseHelper(null);
		try {
			help.createDataBase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
