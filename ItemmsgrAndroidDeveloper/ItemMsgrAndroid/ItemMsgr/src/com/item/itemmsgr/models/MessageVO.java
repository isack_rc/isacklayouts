package com.item.itemmsgr.models;

import java.sql.Timestamp;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
public class MessageVO extends SimpleObservable{
	
	private Long idMessage;
	private Long idConversation;
	private Long idUser;
	private String type; // 0:unknown, 1:msg, 2:map, 3:file. default:1
	private String message;
	private Timestamp dateSent;
	private Timestamp dateReceived;
	private Timestamp updateTS;
	
	public MessageVO(){
		dateSent = new Timestamp(0L);
		dateReceived = new Timestamp(0L);
		updateTS = new Timestamp(0L);
	}
	
	public MessageVO(long idCnv, long idUsr, long idMsg, String txMsg,
			String dateSent, String dateRcv) {
		this.setIdConversation(idCnv);
		this.setIdUser(idUsr);
		this.setIdMessage(idMsg);
		this.setMessage(txMsg);
		try{
			this.setDateReceived(new Timestamp(Long.valueOf(dateRcv)));
		}catch(Exception e ){
			this.setDateReceived(new Timestamp(0));
		}
		try{
			this.setDateSent(new Timestamp(Long.valueOf(dateSent)));
		}catch(Exception e ){
			this.setDateReceived(new Timestamp(0));
		}
	}
	
	public void notifyChange(){
		notifyObservers(this);
	}
	
	public Long getIdMessage() {
		return idMessage;
	}
	public void setIdMessage(Long idMessage) {
		this.idMessage = idMessage;
	}
	public Long getIdConversation() {
		return idConversation;
	}
	public void setIdConversation(Long idConversation) {
		this.idConversation = idConversation;
	}
	public Long getIdUser() {
		return idUser;
	}
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Timestamp getDateSent() {
		return dateSent;
	}
	public void setDateSent(Timestamp dateSent) {
		this.dateSent = dateSent;
	}
	public Timestamp getDateReceived() {
		return dateReceived;
	}
	public void setDateReceived(Timestamp dateReceived) {
		this.dateReceived = dateReceived;
	}
	public Timestamp getUpdateTS() {
		return updateTS;
	}
	public void setUpdateTS(Timestamp updateTS) {
		this.updateTS = updateTS;
	}
	
}
