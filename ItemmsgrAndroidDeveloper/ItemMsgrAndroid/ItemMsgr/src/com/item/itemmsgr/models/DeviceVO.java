package com.item.itemmsgr.models;

import java.sql.Timestamp;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
@SuppressWarnings("rawtypes")
public class DeviceVO extends SimpleObservable{
	
	private Long idDevice;
	private Long idUser;
	private String identifier;
	private String cloudId;
	private String nickname;
	private String model;
	private String version;
	private Timestamp dateBegin;
	private String status;
	private Timestamp updateTS;
	
	public DeviceVO(String identifier, String cloudId, long idUser){
		this.setIdentifier(identifier);
		this.setCloudId(cloudId);
		this.setIdUser(idUser);
		setDateBegin(new Timestamp(System.currentTimeMillis()));
	}
	
	public DeviceVO(){
		setIdDevice(Long.valueOf(0));
		setDateBegin(new Timestamp(System.currentTimeMillis()));
	}
	
	
	public void notifyChange(){
		notifyObservers(this);
	}
	
	public Long getIdDevice() {
		return idDevice;
	}
	public void setIdDevice(Long idDevice) {
		this.idDevice = idDevice;
	}
	public Long getIdUser() {
		return idUser;
	}
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getCloudId() {
		return cloudId;
	}
	public void setCloudId(String cloudId) {
		this.cloudId = cloudId;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Timestamp getDateBegin() {
		return dateBegin;
	}
	public void setDateBegin(Timestamp dateBegin) {
		this.dateBegin = dateBegin;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getUpdateTS() {
		return updateTS;
	}
	public void setUpdateTS(Timestamp updateTS) {
		this.updateTS = updateTS;
	}
	

}
