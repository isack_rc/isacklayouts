package com.item.itemmsgr.models;

import com.item.itemmsgr.activities.OnChangeListener;

public interface EasyObservable<T> {
	
	void addListener(OnChangeListener<T> listener);
	void removeListener(OnChangeListener<T> listener);
	
}