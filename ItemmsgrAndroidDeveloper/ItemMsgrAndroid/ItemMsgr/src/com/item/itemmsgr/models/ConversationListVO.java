package com.item.itemmsgr.models;

import java.util.ArrayList;

@SuppressWarnings("rawtypes")
public class ConversationListVO extends SimpleObservable{
	
	ArrayList<ConversationVO> conversationList;
	
	public ConversationListVO(){
		conversationList = new ArrayList<ConversationVO>();
	}
	
	public void addConversation(ConversationVO newConversation){
		conversationList.add(newConversation);
		notifyObservers(this);
	}
	
	public void removeConversation(ConversationVO conversation){
		conversationList.remove(conversation);
		notifyObservers(this);
	}
	
	public void removeConversation(Long conversationId){
		for(ConversationVO tmp : conversationList){
			if(tmp.getIdConversation().longValue()==conversationId.longValue()){
				conversationList.remove(tmp);
				notifyObservers(this);
			}
		}
	}
}
