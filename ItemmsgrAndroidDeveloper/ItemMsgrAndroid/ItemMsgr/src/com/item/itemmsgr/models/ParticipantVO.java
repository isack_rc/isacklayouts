package com.item.itemmsgr.models;

public class ParticipantVO extends SimpleObservable{
	private long idUser;
	private long idConversation;
	private String nickname;
	
	public void notifyChange(){
		notifyObservers(this);
	}
	
	public long getIdUser() {
		return idUser;
	}
	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}
	public long getIdConversation() {
		return idConversation;
	}
	public void setIdConversation(long idConversation) {
		this.idConversation = idConversation;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

}
