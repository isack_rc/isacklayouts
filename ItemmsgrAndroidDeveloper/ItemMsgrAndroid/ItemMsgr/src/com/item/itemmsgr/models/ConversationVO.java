package com.item.itemmsgr.models;

import java.sql.Timestamp;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
@SuppressWarnings("rawtypes")
public class ConversationVO extends SimpleObservable{
	
	private Long idConversation;
	private String status;
	private String groupname;
	private String badgeConversation;
	private Timestamp updateTS;
	
	public void notifyChange(){
		notifyObservers(this);
	}
	
	public Long getIdConversation() {
		return idConversation;
	}
	public void setIdConversation(Long idConversation) {
		this.idConversation = idConversation;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	public Timestamp getUpdateTS() {
		return updateTS;
	}
	public void setUpdateTS(Timestamp updateTS) {
		this.updateTS = updateTS;
	}
	public String getBadgeConversation() {
		return badgeConversation;
	}
	public void setBadgeConversation(String badgeConversation) {
		this.badgeConversation = badgeConversation;
	}

}
