package com.item.itemmsgr.models;

public interface OnChangeListener<T> {
	void onChange(T model);
}
