package com.item.itemmsgr.models;

import java.sql.Timestamp;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
@SuppressWarnings("rawtypes")
public class ContactVO extends SimpleObservable{
	
	private Long idContact;
	private Long idUser;
	private String nickname;
	private String imgContact;
	private String status;
	private Timestamp updateTS;
	private String favorite;
	private String telephone;
	private String email;
	private String fullName;
	
	public void notifyChange(){
		notifyObservers(this);
	}
	
	public Long getIdContact() {
		return idContact;
	}
	public void setIdContact(Long idContact) {
		this.idContact = idContact;
	}
	public Long getIdUser() {
		return idUser;
	}
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getImgContact() {
		return imgContact;
	}
	public void setImgContact(String imgContact) {
		this.imgContact = imgContact;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getUpdateTS() {
		return updateTS;
	}
	public void setUpdateTS(Timestamp updateTS) {
		this.updateTS = updateTS;
	}
	public String getFavorite() {
		return favorite;
	}
	public void setFavorite(String favorite) {
		this.favorite = favorite;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
}
