package com.item.itemmsgr.models;

import java.sql.Timestamp;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
@SuppressWarnings("rawtypes")
public class UserVO extends SimpleObservable{
	
	public static final int UNREGISTERED =0;
	public static final int UNREGISTERED_REGKEYSENT=1;
	public static final int REGISTERED=2;
	
	private Long idUser;
	private String name;
	private String lastname; 
	private String telephone;
	private String email;
	private String publicKey;
	private String privateKey;
	private String nKey;
	private String regKey;
	private Timestamp updateTS;
	private int pendings;
	private String nickname;
	private String imgPicture;
	private String imgBackground;
	/**
	 * 0= no registrado
	 * 1= regkey enviado, no validado
	 * 2= regkey validado, usuario registrado
	 */
	private int registrationStatus; 
	
	public UserVO(Long idUser, String name, String lastname, String telephone,
			String email, String imgPicture, String imgBackground,
			String nickname, String publicKey, String nKey, String regKey,
			String privateKey, int registrationStatus) {
		super();
		this.idUser = idUser;
		this.name = name;
		this.lastname = lastname;
		this.telephone = telephone;
		this.email = email;
		this.imgPicture = imgPicture;
		this.imgBackground = imgBackground;
		this.nickname = nickname;
		this.publicKey = publicKey;
		this.nKey = nKey;
		this.regKey = regKey;
		this.privateKey = privateKey;
		this.registrationStatus = registrationStatus;
	}
	
	public UserVO(){
		setIdUser(Long.valueOf(0));
	}
	
	public void notifyChange(){
		notifyObservers(this);
	}
	
	public Long getIdUser() {
		return idUser;
	}
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	public String getnKey() {
		return nKey;
	}
	public void setnKey(String nKey) {
		this.nKey = nKey;
	}
	public String getRegKey() {
		return regKey;
	}
	public void setRegKey(String regKey) {
		this.regKey = regKey;
	}
	public Timestamp getUpdateTS() {
		return updateTS;
	}
	public void setUpdateTS(Timestamp updateTS) {
		this.updateTS = updateTS;
	}
	public int getPendings() {
		return pendings;
	}
	public void setPendings(int pendings) {
		this.pendings = pendings;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public int getRegistrationStatus() {
		return registrationStatus;
	}
	public void setRegistrationStatus(int registrationStatus) {
		this.registrationStatus = registrationStatus;
	}
	public String getImgPicture() {
		return imgPicture;
	}
	public void setImgPicture(String imgPicture) {
		this.imgPicture = imgPicture;
	}
	public String getPrivateKey() {
		return privateKey;
	}
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getImgBackground() {
		return imgBackground;
	}

	public void setImgBackground(String imgBackground) {
		this.imgBackground = imgBackground;
	}
	

}
