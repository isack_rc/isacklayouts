package com.item.itemmsgr.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.item.itemmsgr.R;
import com.item.itemmsgr.controllers.RegistrationController;
import com.item.itemmsgr.models.UserVO;
import com.item.itemmsgr.utilities.exceptions.ExceptionUtility;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
@SuppressWarnings("rawtypes")
public class RKConfirmationActivity extends Activity implements OnChangeListener {

	RegistrationController controller;
	ProgressDialog pDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rkconfirmation);
		controller = new RegistrationController();
		MainItem.currentUser.addListener(this);
	}

	public void btnSendRegkeyConfirmation(View view) {
			sendRegkey(MainItem.currentUser);
	}
	
	public void sendRegkey(UserVO user){
		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Por favor espere...");
		pDialog.setCancelable(false);
		pDialog.show();
		controller.handleMessage(RegistrationController.MESSAGE_SENDREGKEYEMAIL, user);
		if (pDialog.isShowing())
        	pDialog.dismiss(); 
	}

	public void btnConfirmationRegkey(View view) {
		// Metodo de validaci�n de los campos a llenar
		if (validateForm() == 1) {
			Toast.makeText(this, " Campos Completos ", Toast.LENGTH_SHORT)
					.show();
			final RKConfirmationActivity mySelf = this;
			
			if(sendRegistrationKey()){
				Intent i = new Intent(mySelf,ListConversationActivity.class);
				startActivity(i);
			}
			else{
				if(MainItem.currentException!=null)
					ExceptionUtility.handleException(this);
				//Intent i = new Intent(mySelf,RKConfirmationActivity.class);
				//startActivity(i);
			}
		}
	}
	
	

	public void btnCancelConfirmation(View view) {
		Intent i = new Intent(this, MainItem.class);
		startActivity(i);
	}

	public void touchRelativeLayout(View view) {
		EditText txtNickname = (EditText) findViewById(R.id.txRegkeyConfirmation);
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(txtNickname.getWindowToken(), 0);

	}

	public int validateForm() {
		EditText txtRegkey = (EditText) findViewById(R.id.txRegkeyConfirmation);
		String strRegkey = txtRegkey.getText().toString();

		int flag = 1;
		if (TextUtils.isEmpty(txtRegkey.getText())) {
			Toast.makeText(this, " Falta llenar campo regkey ",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}

		if (strRegkey.length() < 8 || strRegkey.length() > 8) {
			Toast.makeText(this, "El Regkey debe contener 8 caracteres ",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}
		if (strRegkey.matches("[A-Za-z0-9]+") == false
				|| strRegkey.contains(" ")) {
			Toast.makeText(
					this,
					"Regkey incorrecto con caracteres extra�os o espacios en blanco ",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}
		return flag;
	}

	public boolean sendRegistrationKey() {
		boolean result = false;
		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Please wait...");
		pDialog.setCancelable(false);
		pDialog.show();
		result = controller.handleMessage(RegistrationController.MESSAGE_SENDREGKEYOK, MainItem.currentUser);
		if (pDialog.isShowing())
        	pDialog.dismiss();
		return result;
	}
	
	@Override
	public void onChange(Object user) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateView();
            }
        });
    }
	
	private void updateView() {
    }

}
