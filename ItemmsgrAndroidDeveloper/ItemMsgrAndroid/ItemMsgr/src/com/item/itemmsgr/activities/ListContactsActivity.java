package com.item.itemmsgr.activities;

import com.item.itemmsgr.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class ListContactsActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_add_contact);
	
	}

	public void btnCallAddContact(View view) {
		Toast.makeText(this, "  Boton add contacto ", Toast.LENGTH_SHORT).show();
		// Estas lineas realizan el cambio de contenido del menu pasandole las clases que necesitamos mostrar
		TabLayoutActivity tLAct = new TabLayoutActivity ();
		tLAct.clearAllTabs();
		tLAct.tabsRefresh(ListConversationActivity.class,AddContactActivity.class, SettingsActivity.class,1);
	}
	
	public void btnCallPhoneBookContact(View view) {
		Toast.makeText(this, "  Boton add contacto ", Toast.LENGTH_SHORT).show();
		// Estas lineas realizan el cambio de contenido del menu pasandole las clases que necesitamos mostrar
		TabLayoutActivity tLAct = new TabLayoutActivity ();
		tLAct.clearAllTabs();
		tLAct.tabsRefresh(ListConversationActivity.class,PhoneBookContactActivity.class, SettingsActivity.class,1);
	}
	
}
