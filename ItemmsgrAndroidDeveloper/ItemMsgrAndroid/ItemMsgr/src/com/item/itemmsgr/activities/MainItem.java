package com.item.itemmsgr.activities;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.item.itemmsgr.R;
import com.item.itemmsgr.agents.GcmBroadcastReceiver;
import com.item.itemmsgr.agents.PostTask;
import com.item.itemmsgr.daos.ConversationDAO;
import com.item.itemmsgr.daos.DeviceDAO;
import com.item.itemmsgr.daos.UserDAO;
import com.item.itemmsgr.models.ConversationListVO;
import com.item.itemmsgr.models.DeviceVO;
import com.item.itemmsgr.models.UserVO;
import com.item.itemmsgr.utilities.Constants;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
public class MainItem extends Activity {
	

	public static PostTask postagent;
	public static UserVO currentUser;
	public static DeviceVO currentDevice;
	public static ConversationListVO conversationList;
	private static Context context;
	public static GoogleCloudMessaging gcm;
	public static AtomicInteger msgId = new AtomicInteger();
	public static String regid;
	public static Exception currentException;



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_item);
		setContext(getApplicationContext());
		
		//Check google play services
	/*	if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);
            if (regid.isEmpty()) {
                registerInBackground();
            }
            registerReceiver(new GcmBroadcastReceiver(),new IntentFilter(Constants.GCM_FROM_SERVER));
        } else {
            Log.e("Error de registro a servicio de Mensajes", "No cuenta con los requerimientos mínimos de mensajería");
        }
		*/
		//Load User and conversations, if failed, create new ones.
		UserDAO udb = new UserDAO(getContext());
		DeviceDAO ddb = new DeviceDAO(getContext());
		if(!udb.loadCurrentUser())
			currentUser = new UserVO();
		if(!ddb.loadCurretDevice())
			currentDevice = new DeviceVO();
		if(currentUser.getRegistrationStatus() == UserVO.REGISTERED){
			ConversationDAO cdb=new ConversationDAO(getContext());
			cdb.getConversationList();
		}
		else
			conversationList = new ConversationListVO();
		
	}
	
	
	private boolean checkPlayServices() {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        Log.e("Error de registro a servicio de Mensajes", "El dispositivo no está soportado: "+resultCode);
	        return false;
	    }
	    return true;
	}
	
	private String getRegistrationId(Context context) {
	    final SharedPreferences prefs = getGCMPreferences(context);
	    String registrationId = prefs.getString(Constants.PROPERTY_REG_ID, "");
	    return registrationId;
	}

	private void storeRegistrationId(Context context, String regid) {
	    final SharedPreferences prefs = getGCMPreferences(context);
	    Editor edit = prefs.edit();
	    edit.putString(Constants.PROPERTY_REG_ID, regid);
	    edit.apply();
	}
	
	private SharedPreferences getGCMPreferences(Context context) {
	    return getSharedPreferences(MainItem.class.getSimpleName(),Context.MODE_PRIVATE);
	}
	
	private void registerInBackground() {
	    new AsyncTask<Void, Void, String>() {
	        @Override
	        protected String doInBackground(Void... params) {
	            String msg = "";
	            try {
	                if (gcm == null) {
	                    gcm = GoogleCloudMessaging.getInstance(context);
	                }
	                regid = gcm.register(Constants.SENDER_ID);
	                storeRegistrationId(context, regid);
	            } catch (IOException ex) {
	                msg = "Error :" + ex.getMessage();
	            }
	            return msg;
	        }
	    }.execute(null, null, null);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_item, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.listViewMsg) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// ----------------------- COMPONENTES DE LA PANTALLA --------------------------------//
	public void btnRegister(View view) {
		//Intent i = new Intent(this, RegisterActivity.class);
		Intent i = new Intent(this, TabLayoutActivity.class);
		startActivity(i);
	}

	public void btnRegistered(View view) {
		Intent i = new Intent(this, RegisteredActivity.class);
		startActivity(i);
	}

	public void btnPrivacyNotice(View view){
		Intent i = new Intent(this, PrivacyNotice.class);
		startActivity(i);
		
		
	}
	
	public static Context getContext() {
		return context;
	}

	public static void setContext(Context context) {
		MainItem.context = context;
	}

	
	
}
