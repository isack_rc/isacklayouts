package com.item.itemmsgr.activities;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TabHost;
import android.widget.Toast;
import com.item.itemmsgr.activities.*;
import com.item.itemmsgr.R;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 *
 */
public class ListConversationActivity extends Activity{
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listconversation);
		
		
		
	}
	
	public void btnCallAddConversation(View view) {
		TabLayoutActivity tLAct = new TabLayoutActivity ();
		tLAct.clearAllTabs();
		tLAct.tabsRefresh(AddConversationActivity.class,ListContactsActivity.class, SettingsActivity.class,0);
	
	}


	
	
}
