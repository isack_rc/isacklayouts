package com.item.itemmsgr.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ErrorDialogFragment;
import com.item.itemmsgr.R;
import com.item.itemmsgr.controllers.RegistrationController;
import com.item.itemmsgr.activities.OnChangeListener;
import com.item.itemmsgr.models.UserVO;
import com.item.itemmsgr.utilities.Utilities;
import com.item.itemmsgr.utilities.exceptions.ExceptionUtility;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
@SuppressWarnings("rawtypes")
public class RegisterActivity extends Activity implements OnChangeListener {
	
	RegistrationController controller;
	ProgressDialog pDialog;
	UserVO user=null;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		controller = new RegistrationController();
		MainItem.currentUser.addListener(this);

	}

	
	// Comienza Codigo ItemMsgr
	public void btnRegisterReg(View view) {
		
		// Metodo de validacion de los campos a llenar
		if (validateForm() == 1) {
			user = new UserVO();
			EditText txtNickname = (EditText) findViewById(R.id.txNickname);
			EditText txtName = (EditText) findViewById(R.id.txName);
			EditText txtLastName = (EditText) findViewById(R.id.txLastName);
			EditText txtCel = (EditText) findViewById(R.id.txCel);
			EditText txtEmail = (EditText) findViewById(R.id.txEmail); 
			user.setName(txtName.getText().toString());
			user.setEmail(txtEmail.getText().toString());
			user.setLastname(txtLastName.getText().toString());
			user.setNickname(txtNickname.getText().toString());
			user.setTelephone(txtCel.getText().toString());
			String regkey = Utilities.generateRegkey();
			user.setRegKey(regkey);
			user.setRegistrationStatus(UserVO.UNREGISTERED);
			Toast.makeText(this, " Campos Completos ", Toast.LENGTH_SHORT).show();
			final RegisterActivity mySelf = this;
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
			// Setting Dialog Title
			alertDialog.setTitle("Clave Regkey enviada");
			// Setting Dialog Message
			alertDialog.setMessage("Se ha enviado una clave regKey a tu correo electrónico, utilizala para completar tu registro");
			// Setting Negative "NO" Button
			alertDialog.setNegativeButton("Continuar...",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Codigo para ir a la ventana de Confirmacion
							Intent i = new Intent(mySelf, RKConfirmationActivity.class);
							startActivity(i);
							// Cierra el dialogo
							dialog.cancel();
						}

					});
			// Showing Alert Message
			alertDialog.show();
			MainItem.currentUser = user;
			if(!sendRegkey(user) && MainItem.currentException!= null){
				ExceptionUtility.handleException(this);
			}
		}
	}

	public void btnCalcelReg(View view) {
		Intent i = new Intent(this, MainItem.class);
		startActivity(i);
	}

	public void touchRelativeLayout(View view) {
		EditText txtNickname = (EditText) findViewById(R.id.txNickname);
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(txtNickname.getWindowToken(), 0);
	}


	public boolean sendRegkey(UserVO user){
		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Por favor espere...");
		pDialog.setCancelable(false);
		pDialog.show();
		boolean result = controller.handleMessage(RegistrationController.MESSAGE_SENDREGKEYEMAIL, user);
		if (pDialog.isShowing())
        	pDialog.dismiss(); 
		return result;
	}

	public int validateForm() {
		EditText txtNickname = (EditText) findViewById(R.id.txNickname);
		EditText txtName = (EditText) findViewById(R.id.txName);
		EditText txtLastName = (EditText) findViewById(R.id.txLastName);
		EditText txtCel = (EditText) findViewById(R.id.txCel);
		EditText txtEmail = (EditText) findViewById(R.id.txEmail);
		String strEmail = txtEmail.getText().toString();
		String strCel = txtCel.getText().toString();

		int flag = 1;
		if (TextUtils.isEmpty(txtNickname.getText())) {
			Toast.makeText(this, " Falta llenar campo Nickname ",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}
		if (TextUtils.isEmpty(txtName.getText())) {
			Toast.makeText(this, " Falta llenar campo Nombre",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}
		if (TextUtils.isEmpty(txtLastName.getText())) {
			Toast.makeText(this, " Falta llenar campo Apellidos ",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}
		if (TextUtils.isEmpty(txtCel.getText())) {
			Toast.makeText(this, " Falta llenar campo Celular ",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}
		if (TextUtils.isEmpty(txtEmail.getText())) {
			Toast.makeText(this, " Falta llenar campo correo electr�nico ",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}
		if (strCel.length() < 10 || strCel.length() > 10) {
			Toast.makeText(this, "El celular debe contener 10 digitos ",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}
		if (strCel.matches("[0-9]+") == false || strCel.contains(" ")) {
			Toast.makeText(
					this,
					"El celular no debe contener letras, caracteres raros o espacios en blanco ",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}
		if (strEmail.contains(" ") ){//|| strEmail.matches(".+@.+\\.[a-z]+") == false) {
			Toast.makeText(
					this,
					"El correo electronico no debe contener espacios en blanco",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}
		if ( strEmail.matches(".+@.+\\.[a-z]+") == false) {
			Toast.makeText(
					this,
					"El correo electronico no tiene el formato adecuado",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}
		
		return flag;
	}
	
	@Override
	public void onChange(Object user) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateView();
            }
        });
    }
 
    private void updateView() {
    }

}
