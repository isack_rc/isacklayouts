package com.item.itemmsgr.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.item.itemmsgr.R;
import com.item.itemmsgr.controllers.RegistrationController;
import com.item.itemmsgr.models.UserVO;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
@SuppressWarnings("rawtypes")
public class RegisteredActivity extends Activity implements OnChangeListener {

	RegistrationController controller;
	ProgressDialog pDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registered);
		controller = new RegistrationController();
		MainItem.currentUser.addListener(this);
	}

	UserVO user=null;
	public void btnIdentifyRegistered(View view) {
		if (validateForm() == 1) {
			EditText txtRegkey = (EditText) findViewById(R.id.txContactData);
			EditText txtEmail = (EditText) findViewById(R.id.txEmailRegistered);
			user = new UserVO();
			user.setEmail(txtEmail.getText().toString());
			user.setRegKey(txtRegkey.getText().toString());
			user.setRegistrationStatus(UserVO.UNREGISTERED);
			
			verifyRegistration(user);
							
		}
	} 
	
	public void verifyRegistration(UserVO user){
		pDialog = new ProgressDialog(this);
		pDialog.setMessage("Por favor espere...");
		pDialog.setCancelable(false);
		pDialog.show();
		boolean result = controller.handleMessage(RegistrationController.MESSAGE_EXISTINGUSER, user);
		if (pDialog.isShowing())
        	pDialog.dismiss(); 
		if(result){
			Intent i = new Intent(this, ListConversationActivity.class);
			startActivity(i);
		}
		else{
			//Show error dialog
		}
	}

	public void btnCancelRegistered(View view) {
		Intent i = new Intent(this, MainItem.class);
		startActivity(i);
	}

	public void touchRelativeLayoutConfirmation(View view) {
		EditText txtRegkey = (EditText) findViewById(R.id.txContactData);
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(txtRegkey.getWindowToken(), 0);

	}

	public int validateForm() {
		EditText txtRegkey = (EditText) findViewById(R.id.txContactData);
		EditText txtEmail = (EditText) findViewById(R.id.txEmailRegistered);

		int flag = 1;
		if (TextUtils.isEmpty(txtRegkey.getText())) {
			Toast.makeText(this, " Falta llenar campo Regkey ",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}
		if (TextUtils.isEmpty(txtEmail.getText())) {
			Toast.makeText(this, " Falta llenar campo correo electr�nico ",
					Toast.LENGTH_SHORT).show();
			return ++flag;
		}

		return flag;
	}
	
	@Override
	public void onChange(Object user) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateView();
            }
        });
    }
	
	private void updateView() {
    }

}