package com.item.itemmsgr.activities;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.item.itemmsgr.daos.ContactDAO;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. No part of this
 * software may be reproduced without ITEM's express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. Ninguna
 * porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
public class ContactsActivity extends ListActivity implements OnChangeListener {

	SimpleCursorAdapter mAdapter;
	static Activity main;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		main = this;
		
		// Create a button to display while the list loads
		Button addContact = new Button(this);
		addContact.setText("Agregar Contacto");
		addContact.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				Intent i = new Intent(main, AddContactActivity.class);
				startActivity(i);
			}
			
		});
		addContact.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, Gravity.CENTER));
		getListView().setEmptyView(addContact);
		ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
		root.addView(addContact);

		String[] fromColumns = { "nickname" , "idContact"};
		int[] toViews = { android.R.id.text1 , android.R.id.text2 }; 

		mAdapter = new SimpleCursorAdapter(this,
				android.R.layout.simple_list_item_1, null, fromColumns,
				toViews, 0);
		setListAdapter(mAdapter);
		updateView();
		
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		//TODO show contact profile
	}

	@Override
	public void onChange(Object user) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				updateView();
			}
		});
	}

	private void updateView() {
		ContactDAO cdb= new ContactDAO(MainItem.getContext());
		//TEST
		/*ContactVO cContact = new ContactVO();
		cContact.setNickname("TestNick");
		cContact.setStatus("on");
		cContact.setIdContact(Long.valueOf(1));
		cContact.setTelephone("525501928374");
		cContact.setEmail("test@gmail.ca");
		cContact.setFullName("FullName Test");
		cContact.setFavorite("off");
		cContact.setIdUser(Long.valueOf(2));
		cContact.setImgContact("");
		cContact.setStatus("on");
		cContact.setUpdateTS(new Timestamp(System.currentTimeMillis()));
		
		cdb.saveOrUpdate(cContact);*/
		//TEST END
		Cursor cursor = cdb.getAllByCursor();
		if (cursor != null && cursor.getCount() > 0) 
			mAdapter.swapCursor(cursor);
	}

}