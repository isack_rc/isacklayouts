package com.item.itemmsgr.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.item.itemmsgr.R;

public class AddConversationActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addconversation);
		
	}

	public void btnCallConversation(View view) {
		Toast.makeText(this, " Conversation activity ", Toast.LENGTH_SHORT).show();
		TabLayoutActivity tLAct = new TabLayoutActivity ();
		tLAct.clearAllTabs();
		tLAct.tabsRefresh(ConversationActivity.class,ListContactsActivity.class, SettingsActivity.class,0);
	}
	
	public void btnBackAddConversation(View view){
		TabLayoutActivity tLAct = new TabLayoutActivity ();
		tLAct.clearAllTabs();
		tLAct.tabsRefresh(ListConversationActivity.class,ListContactsActivity.class, SettingsActivity.class,0);
		
		
	}
	
}
