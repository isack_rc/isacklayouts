package com.item.itemmsgr.activities;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TabHost;

import com.item.itemmsgr.R;

public class TabLayoutActivity extends TabActivity{
	public static TabHost mTabHost;
	public static TabLayoutActivity Me;
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tablayout);
		Me = this;
		tabsRefresh(ListConversationActivity.class,ListContactsActivity.class, SettingsActivity.class,0);
		
	}
	

	
	// ------- METODOS QUE UTILIZAN LOS COMPONENTES DE LA PANTALLA -----------------------//
	
	public static void clearAllTabs() {
		mTabHost.setCurrentTab(0); 
		mTabHost.clearAllTabs();
		
		}
	
	
	
	public static void tabsRefresh(Class TabUno, Class TabDos, Class TabTres, int current){
		mTabHost = Me.getTabHost();
		TabHost.TabSpec spec;
		Intent intent;
		
		
		//ListConversation Activity
				intent = new Intent(Me, TabUno);
				spec = mTabHost.newTabSpec("listconversation")
						.setIndicator("Conversaciones")
						.setContent(intent);
				mTabHost.addTab(spec);
				
				
				//ListContacts Activity
				intent = new Intent(Me, TabDos );
				spec = mTabHost.newTabSpec("listcontacts")
						.setIndicator("Contactos")
						.setContent(intent);
				mTabHost.addTab(spec);
				
				//Setting Activity
				intent = new Intent(Me, TabTres );
				spec = mTabHost.newTabSpec("settings")
						.setIndicator("Ajustes")
						.setContent(intent);
				mTabHost.addTab(spec);
				
				//focus TAB
				mTabHost.setCurrentTab(current);
		
		
	}
	
}
