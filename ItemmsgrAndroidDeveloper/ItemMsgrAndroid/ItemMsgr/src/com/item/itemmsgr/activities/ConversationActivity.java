package com.item.itemmsgr.activities;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.item.itemmsgr.R;

public class ConversationActivity extends Activity {
	private com.item.itemmsgr.activities.DiscussArrayAdapter adapter;
	private ListView lv;
	private EditText editText1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.conversation);
		
		lv = (ListView) findViewById(R.id.listView1);
		adapter = new DiscussArrayAdapter(getApplicationContext(), R.layout.listitem_discuss);
		lv.setAdapter(adapter);
		editText1 = (EditText) findViewById(R.id.editText1);
		editText1.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// If the event is a key-down event on the "enter" button
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
					// Perform action on key press
					if(!editText1.getText().toString().isEmpty())
					adapter.add(new OneComment(true, editText1.getText().toString()));
					editText1.setText("");
					return true;
				}
				return false;
			}
		});

		
	}
	
	

	private void addItems(String msj) {
		//Este metodo se puede utilizat para ingresar el texto a la pantalla 
		// El true es del que te envia el msj y el false es tu msj
		adapter.add(new OneComment(true, msj));		
	}
	
	public class OneComment {
		public boolean left;
		public String comment;

		public OneComment(boolean left, String comment) {
			super();
			this.left = left;
			this.comment = comment;
		}

	}
	
	
	public void touchRelativeLayout(View view) {
		Toast.makeText(this, "tocaste el layoutRelative", Toast.LENGTH_SHORT)
		.show();
		EditText editTextVar = (EditText) findViewById(R.id.editText1);
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editTextVar.getWindowToken(), 0);
	}
	
	
}

	
	
