package com.item.itemmsgr.register;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;
import com.item.itemmsgr.R;


public class RKConfirmation extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rkconfirmation);
	} 

	   public void btnSendRegkeyConfirmation(View view) {
	    //    Intent i = new Intent(this, .class ); // Modificar a donde me enviara
	    //    startActivity(i);
	  }
	    public void btnConfirmationRegkey(View view) {
	    	  // Metodo de validaci�n de los campos a llenar
			   if(validateForm() == 1){
				   Toast.makeText(this, " Campos Completos ", Toast.LENGTH_SHORT).show();
				   
				  final RKConfirmation mySelf = this; 
				   AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		            // Setting Dialog Title
			        alertDialog.setTitle("Clave Regkey enviada");
			        // Setting Dialog Message
			        alertDialog.setMessage("Se ha enviado una clave regKey a tu correo electr�nico, utilizala para completar tu registro");
			        // Setting Negative "NO" Button
			        alertDialog.setNegativeButton("Continuar...", new DialogInterface.OnClickListener() {
			            public void onClick(DialogInterface dialog, int which) {
			            	
			                //Metodo para enviar el post y recibir la informaci�n
			            	// mySelf.postSendMail(eMail,regkey);
			            	//Codigo para ir a la ventana de Confirmacion
				            Intent i = new Intent(mySelf, RKConfirmation.class );  
				 		    startActivity(i);
				 		    //Cierra el dialogo
			            	 dialog.cancel();
			            }
			        });
			        // Showing Alert Message
			        alertDialog.show();
			   

			   }  
	    	
	  }
	    public void btnCancelConfirmation(View view) {
	        Intent i = new Intent(this, Register.class );
	        startActivity(i);
	  }
	
	    public void touchRelativeLayout(View view) { 
	    	 EditText txtNickname = (EditText) findViewById(R.id.txRegkeyConfirmation);
	    	InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(txtNickname.getWindowToken(), 0);
	    
	    }
	  // ---------------------------------METODOS QUE UTILIZA EL ITEMMSGR---------------  
	
	    public int validateForm() {   
	    	EditText txtRegkey = (EditText) findViewById(R.id.txRegkeyConfirmation);
	    	String strRegkey = txtRegkey.getText().toString();
	    
			  
	    	int flag = 1;
			   		if(TextUtils.isEmpty(txtRegkey.getText()) ){
			   			Toast.makeText(this, " Falta llenar campo regkey ", Toast.LENGTH_SHORT).show();
			   			return ++flag;
			   		}
		   			
		   		
		   			if(strRegkey.length() < 8 || strRegkey.length() > 8 ){
		   				Toast.makeText(this, "El Regkey debe contener 8 caracteres ", Toast.LENGTH_SHORT).show();
		   				return ++flag;
		   			}
		   			if(strRegkey.matches("[A-Za-z0-9]+") == false || strRegkey.contains(" ")){ 
		   				Toast.makeText(this, "Regkey incorrecto con caracteres extra�os o espacios en blanco ", Toast.LENGTH_SHORT).show();
		   				return ++flag;
		   			}
		   			return flag;
			   }
	    
	    
	    
	    /*    private void postSendMail(String eMail,String regkey) {
		    // Politicas necesarias para la conexion http
		   ProgressDialog pDialog;
       	   pDialog = new ProgressDialog(this);
           pDialog.setMessage("Please wait...");
           pDialog.setCancelable(false);
           pDialog.show();
		   
           EditText txtRegkey = (EditText) findViewById(R.id.txRegkeyConfirmation); 
		   String strRegkey = txtRegkey.getText().toString();
		   
           
		   StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		   StrictMode.setThreadPolicy(policy);
		   String responseString = null; 
		   // Create a new HttpClient and Post Header
		    HttpClient httpclient = new DefaultHttpClient();
		    HttpPost httppost = new HttpPost(Constants.emailRegKeySrvPath);
		    // contacts JSONArray
		    JSONArray contacts = null;
		    try {
		        // Add your data
		        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		        nameValuePairs.add(new BasicNameValuePair("eMail", eMail));
		        nameValuePairs.add(new BasicNameValuePair("regkey", regkey));
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		        // Execute HTTP Post Request
		        HttpResponse response = httpclient.execute(httppost);
		        StatusLine statusLine = response.getStatusLine();
		         if(statusLine.getStatusCode() == HttpStatus.SC_OK){
		                ByteArrayOutputStream out = new ByteArrayOutputStream();
		                response.getEntity().writeTo(out);
		                responseString = out.toString();
		                Log.e("Isaac: ","El responseString es: "+responseString);
		                out.close();
		                try {
		                	JSONObject jsonObj = new JSONObject(responseString); 
		                	contacts = jsonObj.getJSONArray("list");  
		                	JSONObject c = contacts.getJSONObject(0);
		                	String tel = c.getString("telephone");
		                	// 	tmp hashmap for single contact
		                	HashMap<String, String> contact = new HashMap<String, String>();
		                	contact.put("list", tel);
		                	Log.e("Isaac: ", "El valor del telefono es: "+tel);
		                	} catch (JSONException e) {
		                		e.printStackTrace();
		                	} 
	             } else{
	                //Closes the connection.
	                response.getEntity().getContent().close();
	                throw new IOException(statusLine.getReasonPhrase());
	             }
		       
		    } catch (ClientProtocolException e) {
		        // TODO Auto-generated catch block
		    } catch (IOException e) {
		        // TODO Auto-generated catch block
		    }
		    if (pDialog.isShowing())
		    	pDialog.dismiss(); 
		    
		}
*/
}
	    
	    
	    

