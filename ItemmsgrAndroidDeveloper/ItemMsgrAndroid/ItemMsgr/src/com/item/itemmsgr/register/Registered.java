package com.item.itemmsgr.register;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.item.itemmsgr.R;
public class Registered extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registered);
		
	}

	   public void btnIdentifyRegistered(View view) {
		   Log.e("Isaac:","Oprimio el boton!!");
		  
		   StringBuffer sb = new StringBuffer(); 
		   sb.append("......Contact Details....."); 
		   ContentResolver cr = getContentResolver(); 
		   Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		   Log.e("Isaac:","No creo que llegue aki!!");
		   String phone = null;
		   String emailContact = null; 
		   String emailType = null;
		   String image_uri = "";
		  
		   if (cur.getCount() > 0) { while (cur.moveToNext()) 
		   { 
			   String id = cur.getString(cur .getColumnIndex(ContactsContract.Contacts._ID)); 
			   String name = cur .getString(cur .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)); 
			   image_uri = cur .getString(cur .getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)); 
			   if (Integer .parseInt(cur.getString(cur .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) { 
				   System.out.println("name : " + name + ", ID : " + id); 
				   sb.append("\n Contact Name:" + name);
				   Cursor pCur = cr.query( ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { id }, null); 
				   while (pCur.moveToNext()) { 
					   phone = pCur .getString(pCur .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)); 
					   sb.append("\n Phone number:" + phone); System.out.println("phone" + phone); }
				   pCur.close(); Cursor emailCur = cr.query( ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[] { id }, null);
				   while (emailCur.moveToNext()) { 
					   emailContact = emailCur .getString(emailCur .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
					   emailType = emailCur .getString(emailCur .getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE)); 
					   sb.append("\nEmail:" + emailContact + "Email type:" + emailType); 
					   System.out.println("Email " + emailContact + " Email Type : " + emailType); } 
				   emailCur.close(); }
			   if (image_uri != null) { System.out.println(Uri.parse(image_uri));
			   
			   }
		   }
		   }

		     
		        
		//   } // FIN DE LA VALIDACION
		   
		   
		   //  Intent i = new Intent(this, Register.class );
	       // startActivity(i);
	  } // FIN DEL BOTON DE YA ESTOY REGISTRADO
	   
	   
	    public void btnCancelRegistered(View view) {
	        
	  }
	    public void touchRelativeLayoutConfirmation(View view) { 
	    	 EditText txtRegkey = (EditText) findViewById(R.id.txContactData);
	    	InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(txtRegkey.getWindowToken(), 0);
	    
	    }
	    
	    //--------------------------------METODOS LLAMADOS POR LOS BOTONES------------------------
	    public int validateForm() {   
	    	EditText txtRegkey = (EditText) findViewById(R.id.txContactData);
			   EditText txtEmail = (EditText) findViewById(R.id.txEmailRegistered);
			   
	    	int flag = 1;
			   		if(TextUtils.isEmpty(txtRegkey.getText()) ){
			   			Toast.makeText(this, " Falta llenar campo Regkey ", Toast.LENGTH_SHORT).show();
			   			return ++flag;
			   		}
		   			if(TextUtils.isEmpty(txtEmail.getText()) ){
		   				Toast.makeText(this, " Falta llenar campo correo electr�nico ", Toast.LENGTH_SHORT).show();
		   				return ++flag;
		   			}	
		   			
		   			return flag;
			   }



}