package com.item.itemmsgr.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
public class NetworkUtil {
	
	public static boolean hasInternetAccess(Context context) {
		boolean hasInternet = false;
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
			hasInternet = true;
		}
		return hasInternet;
	}
	
}
