package com.item.itemmsgr.utilities;

public class Notification {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
