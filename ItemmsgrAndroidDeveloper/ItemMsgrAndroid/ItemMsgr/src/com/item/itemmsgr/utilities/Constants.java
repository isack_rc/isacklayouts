package com.item.itemmsgr.utilities;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
public class Constants {

	    public final static String EMMSRVURL = "http://192.168.69.180:8080/";//"http://54.186.110.253:8080/";//
	    public final static String PHPSRVPATH = "http://itemmsgr.de/emm_tradeco/";   
	    public final static String JAVASRVPATH = EMMSRVURL + "EMMSrv/";
	    public final static String POSTSRVPATH = EMMSRVURL + "PostEMM/";
	    public final static String PHOTOSRVURL = EMMSRVURL + "PostEMM/";
	    public final static String NOTIFTYPE_MESSAGE = "1";
	    public final static String NOTIFTYPE_INVITATION = "2";
	    public final static String NOTIFTYPE_ACCEPTANCE = "3" ;
	    public final static String NOTIFTYPE_MESSAGEUPDATE = "4";
	    public final static String NOTIFTYPE_ERASE = "5";

	    
	 // Servicios de notificaciones y env�o de correo
	    public final static String notificationsSrvPath = "http://itemmsgr.de/emm_tradeco/Notifications.php";
	    public final static String emailRegKeySrvPath = "http://itemmsgr.de/emm_tradeco/emm_email_regkey.php";

	 // Servicios ligados a m�todos equivalentes en la clase KeyController.java
	    
	    public final static String existingUserRegistrationSrvPath = JAVASRVPATH + "existingUserRegistration.htm";
	    public final static String retrieveClientDataSrvPath = JAVASRVPATH + "retrieveClientData.htm";
	    public final static String saveDeviceSrvPath = JAVASRVPATH + "saveDevice.htm";
	    public final static String newUserRegistrationSrvPath = JAVASRVPATH + "newUserRegistration.htm";
	    public final static String saveNewUserSrvPath = JAVASRVPATH + "saveNewUser.htm"; 
	    public final static String relateContactsSrvPath = JAVASRVPATH + "relateContacts.htm"; 
	    public final static String getContactSrvPath = JAVASRVPATH + "getContact.htm"; 
	    public final static String getContactAndDevicesSrvPath = JAVASRVPATH + "getContactAndDevices.htm";
	    public final static String startContactInvitationSrvPath = JAVASRVPATH + "startContactInvitation.htm";
	    public final static String getIdConversationSrvPath = JAVASRVPATH + "getIdConversation.htm";
	    public final static String getTelephoneListWithStatusSrvPath = JAVASRVPATH + "getTelephoneListWithStatus.htm";
	    
	    public final static String getPendingContactInvitationsSrvPath = JAVASRVPATH + "getPendingContactInvitations.htm";
	    public final static String setPendingContactInvitationSrvPath = JAVASRVPATH + "setPendingContactInvitation.htm";
	    public final static String getAckContactInvitationsSrvPath = JAVASRVPATH + "getAckContactInvitations.htm";
	    
	    public final static String getConversationDataSrvPath = JAVASRVPATH + "getConversationData.htm";
	    public final static String getConversationUpdateSrvPath = JAVASRVPATH + "getConversationUpdate.htm";
	    public final static String getParticipantSrvPath = JAVASRVPATH + "getParticipant.htm";
	    
	    public final static String validateOrCreateConversationSrvPath = JAVASRVPATH + "validateOrCreateConversation.htm";
	    public final static String saveMessageFromConversationSrvPath = JAVASRVPATH + "saveMessageFromConversation.htm";
	    public final static String createGroupConversationSrvPath = JAVASRVPATH + "createGroupConversation.htm";
	    
	    public final static String getCurrentTimestampSrvPath = JAVASRVPATH + "getCurrentTimestamp.htm";
	    public final static String updateUserSrvPath = JAVASRVPATH + "updateUser.htm";
	    public final static String resendRegKeySrvPath = JAVASRVPATH + "resendRegKey.htm";
	    
	    public final static String setFavoriteSrvPath = JAVASRVPATH + "setFavorite.htm";
	    public final static String getNotificationParametersSrvPath = JAVASRVPATH + "getNotificationParameters.htm";
	    public final static String getDevicesSrvPath = JAVASRVPATH + "getDevices.htm";
	    
	    public final static String removeDeviceSrvPath = JAVASRVPATH + "removeDevice.htm";
	    public final static String saveLogSrvPath = JAVASRVPATH + "saveLog.htm";
	    public final static String deleteConversationSrvPath = JAVASRVPATH + "deleteConversation.htm";
	    
	    public final static String removeContactSrvPath = JAVASRVPATH + "removeContact.htm";
	    public final static String validateRegisteringEmailSrvPath = JAVASRVPATH + "validateRegisteringEmail.htm";
	
	    public final static String SENDER_ID = "295493029230";
	    public final static String PROPERTY_REG_ID = "";
	    public final static String GCM_FROM_SERVER = "Google Message From Server";
	    
	    /*   
	    #ifdef DEBUG
	    #define EMMSRVURL "192.168.69.180:8080/"    // DESARROLLO
	    #else
	    #define EMMSRVURL "192.168.0.180:8080/"   // PRODUCCI�N
	    #endif



	    #define EMMSRVPATH "http://" EMMSRVURL
	    // Ruta del server alterno para enviar notificaciones y correos
	    #define PHPSRVPATH "http://itemmsgr.de/emm_tradeco/"
	    // Ruta del server de ItemMsgr (de esta forma se indica la ip en un solo lugar)
	    #define JAVASRVPATH EMMSRVPATH "EMMSrv/"
	    // Ruta del servicio de guardado de fotos (de esta forma se indica la ip en un solo lugar)
	    #define POSTSRVPATH EMMSRVPATH "PostEMM/"
	    // Ruta del servicio de guardado de fotos (de esta forma se indica la ip en un solo lugar)
	    #define PHOTOSRVURL EMMSRVURL "PostEMM/"

	    @implementation Constants

	    const int NOTIFTYPE_MESSAGE = 1;
	    const int NOTIFTYPE_INVITATION = 2;
	    const int NOTIFTYPE_ACCEPTANCE = 3 ;
	    const int NOTIFTYPE_MESSAGEUPDATE = 4;
	    const int NOTIFTYPE_ERASE = 5;

	    const int MSGTYPE_UNKNOWN=0;
	    const int MSGTYPE_MSG=1;
	    const int MSGTYPE_MAP=2;
	    const int MSGTYPE_FILE=3;

	    const NSInteger VC_MESSAGES=0;
	    const NSInteger VC_CONTACTS=1;
	    const NSInteger VC_FAVORITES=2;
	    const NSInteger VC_ADJUSTMENTS=3;

	    NSTimeInterval const TOOLTIP_INTERVAL = 4;

	    // Servicios de notificaciones y env�o de correo
	    const NSString *notificationsSrvPath = (NSString *)CFSTR(PHPSRVPATH "Notifications.php");
	    const NSString *emailRegKeySrvPath = (NSString *)CFSTR(PHPSRVPATH "emm_email_regkey.php");

	    // Servicio default de ItemMsgr. Solo regresa: "Hola Mundo 2"
	    const NSString *javaSrvPath = (NSString *)CFSTR(JAVASRVPATH);

	    // Servicio default de PostEMM para guardar fotos
	    const NSString *postSrvPath = (NSString *)CFSTR(POSTSRVPATH); // con protocolo
	    const NSString *photoSrvPath = (NSString *)CFSTR(PHOTOSRVURL); // sin protocolo

	    // Servicios ligados a m�todos equivalentes en la clase KeyController.java
	     const NSString *existingUserRegistrationSrvPath = (NSString *)CFSTR(JAVASRVPATH "existingUserRegistration.htm");
	     const NSString *retrieveClientDataSrvPath = (NSString *)CFSTR(JAVASRVPATH "retrieveClientData.htm");
	     const NSString *saveDeviceSrvPath = (NSString *)CFSTR(JAVASRVPATH "saveDevice.htm");
	     const NSString *newUserRegistrationSrvPath = (NSString *)CFSTR(JAVASRVPATH "newUserRegistration.htm");
	     const NSString *saveNewUserSrvPath = (NSString *)CFSTR(JAVASRVPATH "saveNewUser.htm");
	     const NSString *relateContactsSrvPath = (NSString *)CFSTR(JAVASRVPATH "relateContacts.htm");
	     const NSString *getContactSrvPath = (NSString *)CFSTR(JAVASRVPATH "getContact.htm");
	     const NSString *getContactAndDevicesSrvPath = (NSString *)CFSTR(JAVASRVPATH "getContactAndDevices.htm");
	     const NSString *startContactInvitationSrvPath = (NSString *)CFSTR(JAVASRVPATH "startContactInvitation.htm");
	     const NSString *getIdConversationSrvPath = (NSString *)CFSTR(JAVASRVPATH "getIdConversation.htm");
	     const NSString *getTelephoneListWithStatusSrvPath = (NSString *)CFSTR(JAVASRVPATH "getTelephoneListWithStatus.htm");
	     const NSString *getPendingContactInvitationsSrvPath = (NSString *)CFSTR(JAVASRVPATH "getPendingContactInvitations.htm");
	     const NSString *setPendingContactInvitationSrvPath = (NSString *)CFSTR(JAVASRVPATH "setPendingContactInvitation.htm");
	     const NSString *getAckContactInvitationsSrvPath = (NSString *)CFSTR(JAVASRVPATH "getAckContactInvitations.htm");
	     const NSString *getConversationDataSrvPath = (NSString *)CFSTR(JAVASRVPATH "getConversationData.htm");
	     const NSString *getConversationUpdateSrvPath = (NSString *)CFSTR(JAVASRVPATH "getConversationUpdate.htm");
	     const NSString *getParticipantSrvPath = (NSString *)CFSTR(JAVASRVPATH "getParticipant.htm");
	     const NSString *validateOrCreateConversationSrvPath = (NSString *)CFSTR(JAVASRVPATH "validateOrCreateConversation.htm");
	     const NSString *saveMessageFromConversationSrvPath = (NSString *)CFSTR(JAVASRVPATH "saveMessageFromConversation.htm");
	     const NSString *createGroupConversationSrvPath = (NSString *)CFSTR(JAVASRVPATH "createGroupConversation.htm");
	     const NSString *getCurrentTimestampSrvPath = (NSString *)CFSTR(JAVASRVPATH "getCurrentTimestamp.htm");
	     const NSString *updateUserSrvPath = (NSString *)CFSTR(JAVASRVPATH "updateUser.htm");
	     const NSString *resendRegKeySrvPath = (NSString *)CFSTR(JAVASRVPATH "resendRegKey.htm");
	     const NSString *setFavoriteSrvPath = (NSString *)CFSTR(JAVASRVPATH "setFavorite.htm");
	     const NSString *getNotificationParametersSrvPath = (NSString *)CFSTR(JAVASRVPATH "getNotificationParameters.htm");
	     const NSString *getDevicesSrvPath = (NSString *)CFSTR(JAVASRVPATH "getDevices.htm");
	    const NSString *removeDeviceSrvPath = (NSString *)CFSTR(JAVASRVPATH "removeDevice.htm");
	    const NSString *saveLogSrvPath = (NSString *)CFSTR(JAVASRVPATH "saveLog.htm");
	    const NSString *deleteConversationSrvPath = (NSString *)CFSTR(JAVASRVPATH "deleteConversation.htm");
	    const NSString *removeContactSrvPath = (NSString *)CFSTR(JAVASRVPATH "removeContact.htm");
	    const NSString *validateRegisteringEmailSrvPath = (NSString *)CFSTR(JAVASRVPATH "validateRegisteringEmail.htm");



	    const NSString *imgProfilePath4Format = @"userImages/d%li/%@imgProfile.jpg";
	    const NSString *imgProfilePrePath = @"userImages/d";
	    const NSString *imgProfileOriginalName = @"imgProfile.jpg";
	    const NSString *imgProfileProfileName = @"imgProfile.jpg"; //@"thumb4imgProfile.jpg";
	    const NSString *imgProfileConversationName = @"imgProfile.jpg"; //@"thumb2imgProfile.jpg";

	    const NSString *serverRelease = @"21 mar 2014";
	*/
}
