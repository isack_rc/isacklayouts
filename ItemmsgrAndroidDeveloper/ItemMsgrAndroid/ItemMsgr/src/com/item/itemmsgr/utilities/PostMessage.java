package com.item.itemmsgr.utilities;

import java.util.List;

import org.apache.http.NameValuePair;
import com.item.itemmsgr.controllers.Controller;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
public class PostMessage {
	private String to;
	private List<NameValuePair> nameValuePairs;
	private Controller origin;
	
	public PostMessage(){}
	
	public PostMessage(String to, List<NameValuePair> nameValuePairs, Controller origin){
		this.setTo(to);
		this.setNameValuePairs(nameValuePairs);
		this.setOrigin(origin);
	}
	
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public List<NameValuePair> getNameValuePairs() {
		return nameValuePairs;
	}
	public void setNameValuePairs(List<NameValuePair> nameValuePairs) {
		this.nameValuePairs = nameValuePairs;
	}

	public Controller getOrigin() {
		return origin;
	}

	public void setOrigin(Controller origin) {
		this.origin = origin;
	}
	
}
