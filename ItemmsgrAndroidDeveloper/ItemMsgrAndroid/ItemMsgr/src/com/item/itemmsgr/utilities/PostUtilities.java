package com.item.itemmsgr.utilities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.item.itemmsgr.activities.MainItem;

import android.os.StrictMode;
import android.util.Log;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
public class PostUtilities {
	
	public static String post(PostMessage toSend) {
		// Politicas necesarias para la conexion http
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		String responseString = null;
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(toSend.getTo());
		HttpResponse response = null;
		List<NameValuePair> nameValuePairs = toSend.getNameValuePairs();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			Log.e("PostUtilities, Post ", "Enviando: "+ nameValuePairs.toString());
			nameValuePairs.add(new BasicNameValuePair("txValidDevId",String.valueOf(MainItem.currentDevice.getIdDevice())));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			// Execute HTTP Post Request
			response = httpclient.execute(httppost);
			StatusLine statusLine = response.getStatusLine();
			if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
				response.getEntity().writeTo(out);
				responseString = out.toString();
				Log.e("PostUtilities, Post ", "El responseString es: " + responseString);
			} else {
				// Closes the connection.
				response.getEntity().getContent().close();
				throw new IOException(statusLine.getReasonPhrase());
			}
		} catch (ClientProtocolException ex) {
			Log.e("SendRegkey ", ex.getStackTrace().toString());
		} catch (IOException ex) {
			Log.e("SendRegkey ", ex.getStackTrace().toString());
		}
		finally{
			if(out!= null){
				try {
					out.close();
				} catch (IOException ex) {
					Log.e("SendRegkey ", ex.getStackTrace().toString());
				}
			}
		}
		return responseString;
	}

}
