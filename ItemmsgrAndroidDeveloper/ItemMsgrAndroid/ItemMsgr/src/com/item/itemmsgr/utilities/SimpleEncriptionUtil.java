package com.item.itemmsgr.utilities;

public class SimpleEncriptionUtil {
	public static char[] key = "e*O]7qvultJ(eId[?kT2`}^GQNKjjIk',-p03%obrDq7eS)b0.;c<L%?l]a>?o[0$f3oL0unwpTTy'B;^-dF;7|'y865gaEIDr>&48e0O|1fw5N*S)-q``,PE~`cv5hM".toCharArray();
	
	public static String encrypt(String originalMessage){
		String encryptedMessageString;
		char[] message =originalMessage.toCharArray();
		char[] encryptedMessage = new char[message.length];
		for(int i=0;i<message.length;i++){
			encryptedMessage[i] = (char)(key[i%128]^message[i]);
		}
		encryptedMessageString = new String(encryptedMessage);
		return encryptedMessageString;
	}

	public static String decrypt(String originalMessage){
		String dencryptedMessageString;
		char[] message =originalMessage.toCharArray();
		char[] dencryptedMessage = new char[message.length];
		for(int i=0;i<message.length;i++){
			dencryptedMessage[i] = (char)(key[i%128]^message[i]);
		}
		dencryptedMessageString = new String(dencryptedMessage);
		return dencryptedMessageString;
	}
}
