package com.item.itemmsgr.utilities.exceptions;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
public class ServerException extends Exception {

	private static final long serialVersionUID = 1L;
	String message = "Server Exception";
	public ServerException(String message){
		super(message);
		this.message = message;
		
	}

}
