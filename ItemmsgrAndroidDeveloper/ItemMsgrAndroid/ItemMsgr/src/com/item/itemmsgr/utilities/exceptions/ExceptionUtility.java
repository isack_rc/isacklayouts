package com.item.itemmsgr.utilities.exceptions;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.item.itemmsgr.activities.MainItem;

public class ExceptionUtility {

	
	public static void handleException(Activity act){
		AlertDialog.Builder errorDialog = new AlertDialog.Builder(act);
		errorDialog.setCancelable(false);
		errorDialog.setTitle("Se ha producido un error");
		errorDialog.setMessage(MainItem.currentException.getMessage());
		errorDialog.setPositiveButton("Aceptar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// Cierra el dialogo
						dialog.cancel();
						MainItem.currentException= null;
					}

				});
		errorDialog.show();
	}
}
