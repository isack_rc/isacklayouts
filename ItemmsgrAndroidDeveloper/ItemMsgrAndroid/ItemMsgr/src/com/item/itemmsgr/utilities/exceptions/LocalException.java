package com.item.itemmsgr.utilities.exceptions;

public class LocalException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	
	public LocalException(String mess){
		super(mess);
	}

}
