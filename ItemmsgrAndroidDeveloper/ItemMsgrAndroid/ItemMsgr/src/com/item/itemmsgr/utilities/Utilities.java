package com.item.itemmsgr.utilities;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
public class Utilities {
	public static final int REGKEY_LENGTH = 8;
	
	public static String generateRegkey(){
		String alphabet  = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
	    String regkey ="";
	    
	    for (int i = 0; i < REGKEY_LENGTH; i++) {
	        int r = (int)(Math.random()* alphabet.length());
	        char c = alphabet.charAt(r);
	        regkey = regkey+String.valueOf(c);
	    }
	    return regkey;
	}

}
