package com.item.itemmsgr.tests;

import android.content.Context;
import android.util.Log;

import com.item.itemmsgr.daos.UserDAO;
import com.item.itemmsgr.models.UserVO;


public class TestDAO {
	public void callDAO(Context ctx){
		
		//UserDAO Test
		UserDAO userdao=new UserDAO(ctx);
		UserVO  uservo=(UserVO)userdao.get(2);
		if(uservo!=null){
			Log.i("GET_USER" , uservo.getName()+"_"+uservo.getLastname());
		}
					
		uservo=new UserVO(null,"STEVEN", "LV", "55123456", "ryan.moore@mycompany.com",
				"/imagePicture.png","/imageBackground.png","ryan",
				"publicKey","nKey", "regKey", "privateKey",1);
		
		userdao.saveOrUpdate(uservo);
		userdao.delete(2);
		
	}
	
}
