package com.item.itemmsgr.controllers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.provider.Settings.Secure;
import android.util.Log;

import com.item.itemmsgr.activities.MainItem;
import com.item.itemmsgr.daos.ContactDAO;
import com.item.itemmsgr.daos.ConversationDAO;
import com.item.itemmsgr.daos.DeviceDAO;
import com.item.itemmsgr.daos.MessageDAO;
import com.item.itemmsgr.daos.ParticipantDAO;
import com.item.itemmsgr.daos.UserDAO;
import com.item.itemmsgr.models.ContactVO;
import com.item.itemmsgr.models.ConversationVO;
import com.item.itemmsgr.models.DeviceVO;
import com.item.itemmsgr.models.MessageVO;
import com.item.itemmsgr.models.ParticipantVO;
import com.item.itemmsgr.models.UserVO;
import com.item.itemmsgr.utilities.Constants;
import com.item.itemmsgr.utilities.PostMessage;
import com.item.itemmsgr.utilities.PostUtilities;
import com.item.itemmsgr.utilities.exceptions.LocalException;
import com.item.itemmsgr.utilities.exceptions.ServerException;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
public class RegistrationController extends Controller{
	
	
	public final static int MESSAGE_SENDREGKEYEMAIL = 0;
	public final static int MESSAGE_SENDREGKEYOK = 1;
	public static final int MESSAGE_EXISTINGUSER = 2;
	public static final int MESSAGE_VALIDATEREGEMAIL = 3;
	public static final int MESSAGE_DEMOSTRESSTEST = 4;
	
	@Override
	public boolean handleMessage(int what, Object data) {
		boolean result = false;
		try{
			switch(what){
				case MESSAGE_SENDREGKEYEMAIL:
					sendRegkeyEmail(((UserVO)data).getEmail(),((UserVO)data).getRegKey());
					MainItem.currentUser.setRegistrationStatus(UserVO.UNREGISTERED_REGKEYSENT);
					result = true;
					break;
				case MESSAGE_SENDREGKEYOK:
					newUserRegistration(((UserVO)data));
					MainItem.currentUser.setRegistrationStatus(UserVO.REGISTERED);
					result = true;
					break;
				case MESSAGE_EXISTINGUSER:
					existingUserRegistration(((UserVO)data));
					MainItem.currentUser.setRegistrationStatus(UserVO.REGISTERED);
					result = true;
					break;
				/*case MESSAGE_DEMOSTRESSTEST:
					try {
						result = existingUserRegistrationStressTest(((UserVO)data));
						if(result)
							MainItem.currentUser.setRegistrationStatus(UserVO.REGISTERED);
						
					} catch (ServerException e) {
						notifyOutboxHandlers(Controller.EXCEPTION, 0, 0, e);
					}
					break;*/
				case MESSAGE_VALIDATEREGEMAIL:
					result = validateRegisteringEmail(((UserVO)data).getEmail());
					break;
			}
		} catch (Exception e) {
			MainItem.currentException = e;
		}
		return result;
	}
	
	private void existingUserRegistration(UserVO user) throws ServerException, LocalException{
		DeviceVO newDevice = new DeviceVO(Secure.getString(MainItem.getContext().getContentResolver(),Secure.ANDROID_ID),MainItem.regid,user.getIdUser());
		newDevice.setModel(android.os.Build.DEVICE);
		newDevice.setNickname(android.os.Build.MODEL);
		newDevice.setVersion(android.os.Build.VERSION.SDK);
		String to = Constants.existingUserRegistrationSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txRegKey", user.getRegKey()));
		nameValuePairs.add(new BasicNameValuePair("txDeviceIdentifier", newDevice.getIdentifier()));
		nameValuePairs.add(new BasicNameValuePair("txEmail", user.getEmail()));
		nameValuePairs.add(new BasicNameValuePair("txDeviceName", newDevice.getNickname()));
		nameValuePairs.add(new BasicNameValuePair("txDeviceModel", newDevice.getModel()));
		nameValuePairs.add(new BasicNameValuePair("txCloudId", newDevice.getCloudId()));
		nameValuePairs.add(new BasicNameValuePair("txDeviceVersion", newDevice.getVersion()));
		String responseString = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if(responseString != null){
			try {
				JSONObject jsonObj = new JSONObject(responseString);
				
				int userId = jsonObj.getInt("userId");
				int deviceId = jsonObj.getInt("deviceId");
				boolean userOwnsDevice = jsonObj.getInt("userOwnsDevice")==1;
				user.setIdUser(Long.valueOf(userId));
				newDevice.setIdDevice(Long.valueOf(deviceId));
				if(userId>0){
		            if(userOwnsDevice){
		            	MainItem.currentDevice = newDevice;
		            	if (!saveInfoClient(userId,newDevice.getIdDevice())){
	        				Log.e("RegistrationController","No se obtuvieron datos del servidor, posible error de comunicaciones");
	        				throw new ServerException("Ha ocurrido un error al obtener sus datos, favor de intentar nuevamente.");
	        			}
		            }
		            else{
		            	Log.e("RegistrationController","El usuario no es dueño del dispositivo");
		            	//Send email to user notifying of registration attempt
		            	throw new ServerException("El regkey o el correo no corresponde a su dispositivo. Favor de intentarlo nuevamente.");
		            	
		            }
				}
				else{
					Log.e("RegistrationController","Usuario no estaba registrado");
					throw new ServerException("El regkey o el correo no corresponde a su dispositivo. Favor de intentarlo nuevamente.");
					
				}
			} catch (JSONException ex) {
				Log.e("RegistrationController: ", ex.getStackTrace().toString());
				throw new LocalException("Registration controller: "+ex.getMessage());
			}
		} 	
	}
	
	/*private boolean existingUserRegistrationStressTest(UserVO user) throws ServerException{
		
		boolean result = false;
		DeviceVO newDevice = new DeviceVO(Secure.getString(MainItem.getContext().getContentResolver(),Secure.ANDROID_ID),MainItem.regid,user.getIdUser());
		newDevice.setModel(android.os.Build.DEVICE);
		newDevice.setNickname(android.os.Build.MODEL);
		newDevice.setVersion(android.os.Build.VERSION.SDK);
		String to = Constants.existingUserRegistrationSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txRegKey", user.getRegKey()));
		nameValuePairs.add(new BasicNameValuePair("txDeviceIdentifier", newDevice.getIdentifier()));
		nameValuePairs.add(new BasicNameValuePair("txEmail", user.getEmail()));
		nameValuePairs.add(new BasicNameValuePair("txDeviceName", newDevice.getNickname()));
		nameValuePairs.add(new BasicNameValuePair("txDeviceModel", newDevice.getModel()));
		nameValuePairs.add(new BasicNameValuePair("txCloudId", newDevice.getCloudId()));
		nameValuePairs.add(new BasicNameValuePair("txDeviceVersion", newDevice.getVersion()));
		String responseString = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if(responseString != null){
			try {
				JSONObject jsonObj = new JSONObject(responseString);
				int userId = jsonObj.getInt("userId");
				int deviceId = jsonObj.getInt("deviceId");
				boolean userOwnsDevice = jsonObj.getInt("userOwnsDevice")==1;
				user.setIdUser(Long.valueOf(userId));
				newDevice.setIdDevice(Long.valueOf(deviceId));
				if(userId>0){
		            if(userOwnsDevice){
		            	result = true;
		            }
		            else{
		            	new ServerException("El regkey o el correo no corresponde a su dispositivo. Favor de intentarlo nuevamente.");
		            	Log.e("RegistrationController","El usuario no es dueño del dispositivo");
		            	//Send email to user notifying of registration attempt
		            }
				}
				else{
					new ServerException("El regkey o el correo no corresponde a su dispositivo. Favor de intentarlo nuevamente.");
					Log.e("RegistrationController","Usuario no estaba registrado");
				}
			} catch (JSONException ex) {
				Log.e("RegistrationController: ", ex.getStackTrace().toString());
			}
		} 	
		return result;
	}*/
	
	private boolean saveInfoClient(long clientId, long deviceId) throws ServerException, LocalException{
		boolean result = false;
		String to = Constants.retrieveClientDataSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txIdUser", String.valueOf(clientId)));
		//nameValuePairs.add(new BasicNameValuePair("txValidDevId", String.valueOf(deviceId)));
		String response = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if (response == null) 
			throw new ServerException("Falló el envío de datos del usuario al servidor");
		else{
			try {
				JSONObject jsonObj = new JSONObject(response);
				JSONObject c = jsonObj.getJSONObject("data");
				//Insert Registered User Data in local database
				JSONObject user =c.getJSONObject("user");
				int userId = user.getInt("idUser");
				String userName = user.getString("name");
				String userLastName = user.getString("lastname");
				String userNickname = user.getString("nickname");
				String userPhone = user.getString("telephone");
				String userEmail = user.getString("email");
				String txtUrl = "userImages/d"+userId+"/imgProfile.jpg";
				UserVO newUser = new UserVO();
				newUser.setEmail(userEmail);
				newUser.setName(userName);
				newUser.setLastname(userLastName);
				newUser.setNickname(userNickname);
				newUser.setTelephone(userPhone);
				newUser.setRegistrationStatus(UserVO.REGISTERED);
				newUser.setImgPicture(txtUrl);
				//TODO: Set public, pivate and nkey values
				newUser.setPublicKey("publickey");
				newUser.setnKey("nkey");
				newUser.setRegKey("regkey");
				newUser.setPrivateKey("privatekey");
				UserDAO udb = new UserDAO(MainItem.getContext());
				udb.saveOrUpdate(newUser);
				MainItem.currentUser = newUser;
				//TODO: Process image by URL (userId, txtUrl)
				
				//Insert Registered Devices Data in local database
				DeviceDAO ddb = new DeviceDAO(MainItem.getContext());
				JSONArray devices =c.getJSONArray("device");
				for(int i=0;i<devices.length();i++){
					JSONObject cdev = devices.getJSONObject(i);
					DeviceVO newDevice = new DeviceVO(cdev.getString("identifier"),cdev.getString("cloudId"),cdev.getLong("idUser"));
					newDevice.setIdDevice(cdev.getLong("idDevice"));
					newDevice.setNickname(cdev.getString("nickname"));
					newDevice.setDateBegin(Timestamp.valueOf(cdev.getString("dateBegin")));
					newDevice.setStatus(cdev.getString("status"));
					newDevice.setModel(cdev.getString("model"));
					newDevice.setVersion(cdev.getString("version"));
					ddb.saveOrUpdate(newDevice);
					if(cdev.getString("identifier").equals(Secure.getString(MainItem.getContext().getContentResolver(),Secure.ANDROID_ID)) && cdev.getString("cloudId").equals(MainItem.regid))
						MainItem.currentDevice = newDevice;
				}
				
				//Insert Registered Contact Data in local database
				ContactDAO cdb = new ContactDAO(MainItem.getContext());
				JSONArray contacts =c.getJSONArray("contact");
				for(int i=0;i<contacts.length();i++){
					JSONObject ccont = contacts.getJSONObject(i);
					ContactVO newContact = new ContactVO();
					newContact.setIdContact(ccont.getLong("idContact"));
					newContact.setNickname(ccont.getString("nickname"));
					newContact.setImgContact(ccont.optString("imgContact"));//Hasta aqui ok
					newContact.setStatus(ccont.getString("status"));
					newContact.setTelephone(ccont.getString("telephone"));
					newContact.setEmail(ccont.getString("email"));
					newContact.setFullName(ccont.getString("fullName"));
					newContact.setFavorite(ccont.getString("favorite"));
					cdb.saveOrUpdate(newContact);
				}
				
				//Insert Registered Contact Data in local database
				ParticipantDAO pdb = new ParticipantDAO(MainItem.getContext());
				JSONArray participants =c.getJSONArray("participant");
				for(int i=0;i<participants.length();i++){
					JSONObject cpart = participants.getJSONObject(i);
					ParticipantVO newParticipant = new ParticipantVO();
					newParticipant.setIdUser(cpart.getLong("idUser"));
					newParticipant.setIdConversation(cpart.getLong("idConversation"));
					newParticipant.setNickname(cpart.getString("nickname"));
					pdb.saveOrUpdate(newParticipant);
				} 
				
				//Insert Registered Conversation Data in local database
				ConversationDAO condb = new ConversationDAO(MainItem.getContext());
				JSONArray conversations =c.getJSONArray("conversation");
				for(int i=0;i<conversations.length();i++){
					JSONObject cconv = conversations.getJSONObject(i);
					ConversationVO newConversation = new ConversationVO();
					String convGroupname = cconv.optString("groupname");
					if(convGroupname != null && !convGroupname.isEmpty()){
						newConversation.setGroupname(convGroupname);
					}
					newConversation.setIdConversation(cconv.getLong("idConversation"));
					newConversation.setStatus(cconv.getString("status"));
					condb.saveOrUpdate(newConversation);
				} 
				
				//Insert Message Data in local database
				MessageDAO mdb = new MessageDAO(MainItem.getContext());
				JSONArray messages =c.getJSONArray("message");
				for(int i=0;i<messages.length();i++){
					JSONObject cmess = messages.getJSONObject(i);
					MessageVO newMessage = new MessageVO(cmess.getLong("idConversation"),
							cmess.getLong("idUser"),cmess.getLong("idMessage"),cmess.getString("message"),
							cmess.getString("dateSend"),"0");
					newMessage.setType(cmess.getString("type"));
					mdb.saveOrUpdate(newMessage);
				} 
				result = true;
				Log.i("Registered User","Registered User data saved in local db. UserId: "+ MainItem.currentUser.getIdUser()+ " Phone: "+ MainItem.currentUser.getTelephone()+ " Email: "+MainItem.currentUser.getEmail()+" DeviceId:" + MainItem.currentDevice.getIdDevice());
			}catch (JSONException ex) {
				Log.e("RegistrationController: ", ex.getStackTrace().toString());
				throw new LocalException("Registration controller: "+ex.getMessage());
			}
		}
		return result;
	}
	
	private void sendRegkeyEmail(String email, String regkey) throws ServerException{
		String to = Constants.emailRegKeySrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("eMail", email));
		nameValuePairs.add(new BasicNameValuePair("regkey", regkey));
		String response = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if (response == null || !response.contains("sent!")) 
			throw new ServerException("Falló el envío de correo con clave de registro");
	}
	
	private boolean validateRegisteringEmail(String email) throws ServerException{
		String to = Constants.validateRegisteringEmailSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txEmail", email));
		String response = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if (response == null || response.contains("success\":\"0"))
			throw new ServerException("Falló el envío de correo con clave de registro");
		else 
			return true;
	}

	private void newUserRegistration(UserVO user) throws ServerException, LocalException {
		DeviceVO newDevice = new DeviceVO(Secure.getString(MainItem.getContext().getContentResolver(),Secure.ANDROID_ID),MainItem.regid,user.getIdUser());
		newDevice.setModel(android.os.Build.DEVICE);
		newDevice.setNickname(android.os.Build.MODEL);
		newDevice.setVersion(android.os.Build.VERSION.SDK);
		String to= Constants.newUserRegistrationSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txTelephone", user.getTelephone()));
		nameValuePairs.add(new BasicNameValuePair("txDeviceIdentifier", newDevice.getIdentifier() ));
		nameValuePairs.add(new BasicNameValuePair("txCloudId", newDevice.getCloudId()));
		String responseString = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if(responseString != null && !responseString.contains("success\":\"0")){
			try {
				JSONObject c = new JSONObject(responseString);
				int userId = c.getInt("userId");
				int deviceId = c.getInt("deviceId");
				boolean userOwnDevice = c.getInt("userOwnsDevice")==1;
				
				if(userId>0){
					if(deviceId>0){
						if(userOwnDevice){
							throw new ServerException("Registro inválido, ya está registrado");
						}
						else{
							throw new ServerException("Registro inválido, este dispositivo ya esta registrado con otro usuario");
						}
					}
					else{
						throw new ServerException("Registro inválido, el usuario ya se encuentra registrado");
					}
				}
				else{
					if(deviceId>0){
						throw new ServerException("Registro inválido, Este dispositivo ya esta registrado con otro usuario");
					}
					else{ //Save New User Data
						MainItem.currentUser = user;
						MainItem.currentDevice = newDevice;
						saveNewUserData();
					}
				}
			} catch (JSONException ex) {
				Log.e("RegistrationController: ", ex.getStackTrace().toString());
				throw new LocalException("Registration controller: "+ex.getMessage());
			}
		}
	}
	
	/**
	 * This is a synchronious call to Post, we are not using an AsyncTask as
	 * we want the flow of this registration to be sequential. 
	 * @throws LocalException 
	 */
	public void saveNewUserData() throws ServerException, LocalException{
		UserVO user = MainItem.currentUser;
		DeviceVO device = MainItem.currentDevice;
		String to= Constants.saveNewUserSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txNameUser", user.getName()));
		nameValuePairs.add(new BasicNameValuePair("txLastname", user.getLastname() ));
		nameValuePairs.add(new BasicNameValuePair("txTelephone", user.getTelephone()));
		nameValuePairs.add(new BasicNameValuePair("txEmail", user.getEmail()));
		nameValuePairs.add(new BasicNameValuePair("txCloudId", device.getCloudId() ));
		nameValuePairs.add(new BasicNameValuePair("txPublicKey", "855756"));
		nameValuePairs.add(new BasicNameValuePair("txN", "757655"));
		nameValuePairs.add(new BasicNameValuePair("txRegKey", user.getRegKey() ));
		nameValuePairs.add(new BasicNameValuePair("txNickname", user.getNickname()));
		nameValuePairs.add(new BasicNameValuePair("txDeviceIdentifier", device.getIdentifier()));
		nameValuePairs.add(new BasicNameValuePair("txDeviceName", device.getNickname() ));
		nameValuePairs.add(new BasicNameValuePair("txDeviceModel", device.getModel()));
		nameValuePairs.add(new BasicNameValuePair("txDeviceVersion", device.getVersion()));
		String response = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if(response != null && !response.contains("success\":\"0")){
			try {
				JSONObject c = new JSONObject(response);
				int userId = c.getInt("idUser");
				int deviceId = c.getInt("idDevice");
				MainItem.currentUser.setIdUser(Long.valueOf(userId));
				MainItem.currentDevice.setIdDevice(Long.valueOf(deviceId));
				UserDAO udb = new UserDAO(MainItem.getContext());
				udb.saveOrUpdate(user);
				DeviceDAO ddb = new DeviceDAO(MainItem.getContext());
				ddb.saveOrUpdate(device);
			} catch (JSONException e) {
				Log.e("RegistrationController: ", e.getStackTrace().toString());
				throw new LocalException("Registro fallido, no se pudo guardar el usuario o dispositivo");
			}
		}
		else{
			Log.e("RegistrationController","No se pudo guardar el usuario o dispositivo");
			throw new ServerException("Registro fallido, no se pudo guardar el usuario o dispositivo");
		}
	}

}
