package com.item.itemmsgr.controllers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.item.itemmsgr.activities.MainItem;
import com.item.itemmsgr.daos.ContactDAO;
import com.item.itemmsgr.daos.ConversationDAO;
import com.item.itemmsgr.daos.MessageDAO;
import com.item.itemmsgr.daos.ParticipantDAO;
import com.item.itemmsgr.models.ContactVO;
import com.item.itemmsgr.models.ConversationVO;
import com.item.itemmsgr.models.MessageVO;
import com.item.itemmsgr.models.ParticipantVO;
import com.item.itemmsgr.utilities.Constants;
import com.item.itemmsgr.utilities.Notification;
import com.item.itemmsgr.utilities.PostMessage;
import com.item.itemmsgr.utilities.PostUtilities;
import com.item.itemmsgr.utilities.exceptions.LocalException;
import com.item.itemmsgr.utilities.exceptions.ServerException;
 	
/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
public class ConversationController extends Controller{
	
	public static final int REGISTER_NEW_MESSAGE = 3;
	public static final int DELETE_CONVERSATION = 2;
	public static final int UPDATE_CONVERSATION = 1;
	
	@Override
	public boolean handleMessage(int what, Object data) {
		boolean result = true;
		switch(what){
		case REGISTER_NEW_MESSAGE:
			try {
				registerNewMessage((Notification) data);
			} catch(Exception e){
				Log.e("Message Controller", e.getMessage());
				notifyOutboxHandlers(Controller.EXCEPTION, 0, 0, e);
			}
			break;
		case DELETE_CONVERSATION:
			try {
				//contact object must already contain the telephone from the txtTelephone in the activit
				deleteConversation(((Long)data));
			} catch (ServerException e) {
				Log.e("Message Controller", e.getMessage());
				notifyOutboxHandlers(Controller.EXCEPTION, 0, 0, e);
			}
			break;
		case UPDATE_CONVERSATION:
			try {
				updateConversation(((ConversationVO)data));
			} catch (Exception e) {
				Log.e("Message Controller", e.getMessage());
				notifyOutboxHandlers(Controller.EXCEPTION, 0, 0, e);
			}
			break;
	};
		return result;
	}

	private void registerNewMessage(Notification newMessage) throws ServerException, LocalException {
		JSONArray jsonResponse = null;
		String messageString = newMessage.getMessage(); 
		if(messageString != null){
			
			try {
				JSONObject jsonObj = new JSONObject(messageString);
				jsonResponse = jsonObj.getJSONArray("list");
				JSONObject c = jsonResponse.getJSONObject(0);
				JSONArray infoMessages = c.getJSONArray("infoMessages");
				String cnv = c.getString("cnv");
				JSONArray participants = c.getJSONArray("participants");
				if(cnv != null){
					jsonObj = new JSONObject(cnv);
					jsonResponse = jsonObj.getJSONArray("list");
					c = jsonResponse.getJSONObject(0);
					long idCnv = c.getLong("idConversation");
					String groupname = c.getString("groupname");
					if(groupname!=null) {
						findOrCreateConversation(idCnv,groupname);
					}
					else{
						findOrCreateConversation(idCnv,null);
					}
					
				}
				if(participants != null){
					ParticipantDAO pdb = new ParticipantDAO(MainItem.getContext());
					for(int i =0;i<participants.length();i++){
						jsonObj = new JSONObject(participants.getString(i));
						jsonResponse = jsonObj.getJSONArray("list");
						c = jsonResponse.getJSONObject(0);
						long idUsrPart = c.getLong("idUser");
						
						if(idUsrPart!=MainItem.currentUser.getIdUser()) {
							long idCnv = c.getLong("idConversation");
							String nickname = c.getString("nickname");
							pdb.findOrCreateParticipant(idCnv,nickname);
						}
					}
				}
				if(infoMessages != null){
					for(int i =0;i<infoMessages.length();i++){
						jsonObj = new JSONObject(participants.getString(i));
						jsonResponse = jsonObj.getJSONArray("list");
						c = jsonResponse.getJSONObject(0);
						long idCnv = c.getLong("idConversation");
						long idUsr = c.getLong("idUser");
						long idMsg = c.getLong("idMessage");
						String txMsg = c.getString("message");
						String dateSent = c.getString("dateSend");
						String dateRcv = c.getString("dateReceived");
						MessageVO nm = new MessageVO(idCnv,idUsr,idMsg,txMsg,dateSent,dateRcv);
						MessageDAO mdb = new MessageDAO(MainItem.getContext());
						mdb.saveOrUpdate(nm);
					}
				}
				
			}
			catch(JSONException je){
				throw new LocalException("Register new message : Error posting new message");
			}
		}
	}

	private ConversationVO findOrCreateConversation(long idCnv, String groupname) {
		ConversationDAO cdb = new ConversationDAO(MainItem.getContext());
		ConversationVO conv = (ConversationVO)cdb.get(idCnv);
		if(conv == null){
			conv = new ConversationVO();
			conv.setGroupname(groupname);
			conv.setIdConversation(idCnv);
			//cdb.saveOrUpdate(conv);
		}
		return conv;
	}

	private void deleteConversation(Long idConversation) throws ServerException{
		
		String to = Constants.deleteConversationSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txIdConversation", String.valueOf(idConversation)));
		
		JSONArray jsonResponse = null;
		String responseString = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if(responseString != null){
			try {
				JSONObject jsonObj = new JSONObject(responseString);
				jsonResponse = jsonObj.getJSONArray("list");
				JSONObject c = jsonResponse.getJSONObject(0);
				String success = c.getString("success");
				String error = c.getString("errMsg");
				if(success.equals("1")){
					//ConversationDAO.delete(idConversation);
					//MessageDAO.deleteByConversationId(idConversation);
					//PaticipantDAO.deleteByConversationId(idConversation);
					//MainItem.conversationList.remove(idConversation);
				}
				else
					throw new ServerException("No se pudo eliminar la conversacion: "+error);
			}
			catch(JSONException je){
			}
		}
		else
			throw new ServerException("No se pudo eliminar la conversacion");
	}
	
	private void updateConversation(ConversationVO conversation) throws ServerException, LocalException{
		rechargeBubbleData(conversation.getIdConversation());
		ConversationDAO cdb = new ConversationDAO(MainItem.getContext());
		String to = Constants.getConversationDataSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txIdConversation", String.valueOf(conversation.getIdConversation())));
		long lastIdMessage = cdb.getLastIdMessage(conversation.getIdConversation());
		nameValuePairs.add(new BasicNameValuePair("txLastIdMessage",String.valueOf(lastIdMessage)));
		nameValuePairs.add(new BasicNameValuePair("txIdUser",String.valueOf(MainItem.currentUser.getIdUser())));
		JSONArray jsonResponse = null;
		String responseString = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if(responseString != null && responseString.contains("success\":\"1")){
			try {
				JSONObject jsonObj = new JSONObject(responseString);
				jsonResponse = jsonObj.getJSONArray("list");
				JSONObject c = jsonResponse.getJSONObject(0);
				String success = c.getString("success");
				String error = c.getString("error");
				if(success.equals("1")){
					JSONArray infoMessages = c.getJSONArray("messages");
					rechargeBubbleDataServer(infoMessages);
					JSONArray infoParticipants = c.getJSONArray("participants");
					ParticipantDAO pdb = new ParticipantDAO(MainItem.getContext());
					for(int i =0;i<infoParticipants.length();i++){
						c = infoParticipants.getJSONObject(i);
						long idUsrPart = c.getLong("idUser");
						
						if(idUsrPart!=MainItem.currentUser.getIdUser()) {
							long idCnv = c.getLong("idConversation");
							String nickname = c.getString("nickname");
							ParticipantVO part = pdb.findOrCreateParticipant(idCnv,nickname);
							pdb.saveOrUpdate(part);
						}
					}
				}
				else
					throw new ServerException("No se pudo cargar la conversacion: "+error);
			}
			catch(JSONException je){
			}
		}
		else if(responseString.contains("success\":\"0")){
			newConversation(conversation,0L);//TODO: get id from participant
		}
		else
			throw new ServerException("No se pudo cargar la conversacion");
	}
	
	private void newConversation(ConversationVO conversation, long idContact) throws ServerException, LocalException{
		
		String to ="validateOrCreateConversationSrvPath";
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txIdUser", String.valueOf(MainItem.currentUser.getIdUser())));
		nameValuePairs.add(new BasicNameValuePair("txIdContact", String.valueOf(idContact)));
		
		String responseString = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if(responseString != null && responseString.contains("success\":\"1")){
			try {
				JSONObject c = new JSONObject(responseString);
				long idConversation = c.getLong("idConversation");
				ConversationDAO condb = new ConversationDAO(MainItem.getContext());
				ConversationVO newConversation = new ConversationVO();
				newConversation.setIdConversation(idConversation);
				newConversation.setStatus("on");
				condb.saveOrUpdate(newConversation);
				long idUsrPart = idContact;
				ContactDAO cdb = new ContactDAO(MainItem.getContext());
				ContactVO participc = (ContactVO)cdb.get(idUsrPart);
				String nickname ="NewParticipant";
				if(participc!=null)
					nickname = participc.getNickname();
				else{
					to = "getParticipantSrvPath";
					nameValuePairs.add(new BasicNameValuePair("txIdConversation", String.valueOf(idConversation)));
					nameValuePairs.add(new BasicNameValuePair("txLastIdUser", String.valueOf(idContact)));
					responseString = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
					if(responseString != null && !responseString.isEmpty()){
						c = new JSONObject(responseString);
						nickname = c.getString("nickname");
					}
				}
				ParticipantDAO pdb = new ParticipantDAO(MainItem.getContext());
				ParticipantVO newPart = pdb.findOrCreateParticipant(idConversation, nickname);
				pdb.saveOrUpdate(newPart);
				
				rechargeBubbleDataServer(c.getJSONArray("messages"));
					
			}
			catch(JSONException je){
			}
		}
		else
			throw new ServerException("No se pudo eliminar la conversacion");
	}
	
	public void rechargeBubbleData(long conversationId){
		MessageDAO mdb = new MessageDAO(MainItem.getContext());
		ArrayList<MessageVO> messages = mdb.getMessageListByConversationId(conversationId);
		for(int i=0; i<messages.size(); i++){
			MessageVO mess = messages.get(i);
			//Notify view to update message data and image
		}
	}
	
	public void rechargeBubbleDataServer(JSONArray messages) throws LocalException{
		JSONObject mess;
		MessageDAO mdb = new MessageDAO(MainItem.getContext());
		for(int i=0; i<messages.length(); i++){
			try {
				mess = messages.getJSONObject(i);
				MessageVO newMessage = new MessageVO();
				newMessage.setIdMessage(mess.getLong("idMessage"));
				newMessage.setIdConversation(mess.getLong("idConversation"));
				newMessage.setIdUser(mess.getLong("idUser"));
				newMessage.setType(mess.optString("type"));
				newMessage.setMessage(mess.getString("message"));
				newMessage.setDateSent(new Timestamp(mess.optLong("dateSend")));
				newMessage.setDateReceived(new Timestamp(System.currentTimeMillis()));
				mdb.saveOrUpdate(newMessage);
			} catch (JSONException e) {
				throw new LocalException("Recharge Bubble Server"+e.getMessage());
			}
			//Check dateallowed
			//Notify view to update message data and image
		}
	}
	
	public void getMessagesFromServer(int idConversation) throws ServerException, LocalException{
		
		ConversationDAO cdb = new ConversationDAO(MainItem.getContext());
		String to = Constants.getConversationDataSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txIdConversation", String.valueOf(Long.valueOf(idConversation))));
		long lastIdMessage = cdb.getLastIdMessage(Long.valueOf(idConversation));
		nameValuePairs.add(new BasicNameValuePair("txLastIdMessage",String.valueOf(lastIdMessage)));
		nameValuePairs.add(new BasicNameValuePair("txIdUser",String.valueOf(MainItem.currentUser.getIdUser())));
		
		String responseString = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if(responseString != null && responseString.contains("success\":\"1")){
			try {
				JSONObject jsonObj = new JSONObject(responseString);
				JSONArray infocounts = jsonObj.getJSONArray("ACK");
				if(infocounts != null){
					int idContact;
					String nickname;
	                String urlContact;
	                String email;
	                String telephone;
	                String fullName;
	                
	                ContactDAO condb = new ContactDAO(MainItem.getContext());
					for(int i=0;i<infocounts.length();i++){
						JSONObject c = infocounts.getJSONObject(i);
						idContact = c.getInt("idContact");
						nickname = c.getString("nickname");
						urlContact = c.optString("imgContact");
						if(urlContact.isEmpty())
							urlContact = "userImages/"+idContact+"ld/imgProfile.jpg";
						email = c.getString("email");
						telephone = c.getString("telephone");
						fullName = c.getString("fullName");
						int resultContact = condb.findContact(idContact);
						if(resultContact<=0){
							ContactVO tmp = new ContactVO();
							tmp.setEmail(email);
							tmp.setImgContact(urlContact);
							tmp.setTelephone(telephone);
							tmp.setFullName(fullName);
							tmp.setIdUser(Long.valueOf(idContact));
							tmp.setNickname(nickname);
							condb.saveOrUpdate(tmp);
						}
						//Process image by url - saveContactImage
					}
					//Notify contact table
				}
				JSONArray participants = jsonObj.getJSONArray("participants");
				//TODO check participants
				JSONArray infomessages = jsonObj.getJSONArray("messages");
				MessageDAO mdb = new MessageDAO(MainItem.getContext());
				if(infomessages != null){
					for(int i=0; i<infomessages.length(); i++) {
						JSONObject mess = infomessages.getJSONObject(i);
						long idUsrPart = mess.getLong("idUser");
						if(idUsrPart != MainItem.currentUser.getIdUser()){
							String nickname = mess.getString("nickname");
							String txMsg = mess.getString("message");
							String message = nickname +" : "+txMsg;
							//mdb.saveOfUpdate(message)
							//CREATE BANNER, NOTIFY VIEW
						}
					}
					ConversationVO conversation = (ConversationVO)cdb.get(idConversation);
					conversation.setStatus("on");
					conversation.setBadgeConversation(String.valueOf(infomessages.length()));
					cdb.saveOrUpdate(conversation);
				}
			}
			catch(JSONException je){
				Log.e("ConversationController: ", je.getStackTrace().toString());
				throw new LocalException("Error procesando datos del servidor");
			}
		}
		else{
			Log.e("ConversationController: ", "No se pudieron agregar los mensajes a la conversacion");
			throw new ServerException("No se pudieron agregar los mensajes a la conversacion");
		}
	}
	
}
