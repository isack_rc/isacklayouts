package com.item.itemmsgr.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.item.itemmsgr.activities.MainItem;
import com.item.itemmsgr.models.ContactVO;
import com.item.itemmsgr.models.UserVO;
import com.item.itemmsgr.utilities.Constants;
import com.item.itemmsgr.utilities.PostMessage;
import com.item.itemmsgr.utilities.PostUtilities;
import com.item.itemmsgr.utilities.exceptions.ServerException;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
public class ContactController extends Controller{

	public static final int SEND_CONTACT_INVITATION = 0;
	public static final int SAVE_FAVORITE = 1;
	public static final int REMOVE_CONTACT = 2;
	public static final int ACK_CONTACT_INVITATION = 3;
	
	@Override
	public boolean handleMessage(int what, Object contact) {
		boolean result = false;
		switch(what){
			case SEND_CONTACT_INVITATION:
				try {
					//contact object must already contain the telephone from the txtTelephone in the activit
					result = sendContactInvitation(((UserVO)contact));
				} catch (ServerException e) {
					e.printStackTrace();
				}
				break;
			case SAVE_FAVORITE:
				try {
					result = saveFavorite(((ContactVO)contact));
				} catch (ServerException e) {
					e.printStackTrace();
				}
				break;
			case REMOVE_CONTACT:
				try {
					result = removeContact(((ContactVO)contact));
				} catch (ServerException e) {
					e.printStackTrace();
				}
				break;
			case ACK_CONTACT_INVITATION:
				try {
					//contact object must contain Id from notification
					result = ackContactInvitation((ContactVO)contact);
				} catch (ServerException e) {
					e.printStackTrace();
				}
				break;
		};
		return result;
	}
	
	private boolean removeContact(ContactVO contact) throws ServerException{
		boolean result = false;
		String to = Constants.removeContactSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txIdUser", String.valueOf(MainItem.currentUser.getIdUser())));
		nameValuePairs.add(new BasicNameValuePair("txIdContact", String.valueOf(contact.getIdUser())));
		JSONArray jsonResponse = null;
		String responseString = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if(responseString != null){
			try {
				JSONObject jsonObj = new JSONObject(responseString);
				jsonResponse = jsonObj.getJSONArray("list");
				JSONObject c = jsonResponse.getJSONObject(0);
				String success = c.getString("success");
				String error = c.getString("errMsg");
				if(success.equals("1")){
					//ContactsDAO.delete(contact);
					//String[] conversations = ConversationDAO.selectFromId(contact.getIdUser());
					//ConversationDAO.delete(conversations);
					result = true;
				}
				else
					throw new ServerException("No se pudo eliminar el contacto: "+error);
			}
			catch(JSONException je){
			}
		}
		else
			throw new ServerException("No se pudo eliminar el contacto");
		return result;
	}
	
	private boolean saveFavorite(ContactVO contact) throws ServerException{
		boolean result = false;
		String to = Constants.setFavoriteSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txIdUser", String.valueOf(MainItem.currentUser.getIdUser())));
		nameValuePairs.add(new BasicNameValuePair("txIdContact", String.valueOf(contact.getIdContact())));
		nameValuePairs.add(new BasicNameValuePair("txFavorite", String.valueOf(contact.getFavorite())));
		String responseString = PostUtilities.post(new PostMessage(to, nameValuePairs, this));
		if(responseString==null){
			try {
				JSONObject c = new JSONObject(responseString);
				String success = c.getString("success");
				if(success.equals("1")){
					result = true;
					//ContactDAO.saveOrUpdate(contact);
				}
				else
					throw new ServerException("No se pudo cambiar el estatus de tu contacto");
			}
			catch(JSONException je){
			}
		}
		else
			throw new ServerException("No se pudo cambiar el estatus de tu contacto");
		return result;
	}
	
	private boolean sendContactInvitation(UserVO contact) throws ServerException{
		boolean result = false;
		String to = Constants.startContactInvitationSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txUserIdentifier", String.valueOf(contact.getTelephone())));
		nameValuePairs.add(new BasicNameValuePair("txIdUser", String.valueOf(MainItem.currentUser.getIdUser())));
		String responseString = PostUtilities.post(new PostMessage(to, nameValuePairs, this));
		if(responseString!=null){
			try {
				JSONObject c = new JSONObject(responseString);
				String success = c.getString("success");
				String error = c.optString("errMsg");
				if(success.equals("1")){
					result = true;
					String token = c.getString("tokens");
					String nick = c.optString("nickname");
					sendContactNotification(token, Constants.NOTIFTYPE_INVITATION, nick);
					//Invitacion enviada notificar a Vista ContactActivity
				}
				else
					throw new ServerException("No se pudo enviar tu invitacion: "+error);
			}
			catch(JSONException je){
			}
		}
		else
			throw new ServerException("No se pudo enviar tu invitacion: ");
		return result;
	}
	
	private boolean sendContactNotification(String tokens, String ntype, String nick) throws ServerException{
		boolean result = false;
		String to = Constants.notificationsSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("ntype", ntype));
		nameValuePairs.add(new BasicNameValuePair("message", "Haz recibido una invitación"));
		nameValuePairs.add(new BasicNameValuePair("tokens", tokens));
		nameValuePairs.add(new BasicNameValuePair("senderNick", nick));
		String responseString = PostUtilities.post(new PostMessage(to, nameValuePairs, this));
		if(responseString!=null){
			try {
				JSONObject c = new JSONObject(responseString);
				String success = c.getString("success");
				String error = c.getString("errMsg");
				if(success.equals("1")){
					result = true;
					//Notificacion enviada, notificar a Vista ContactActivity
				}
				else
					throw new ServerException("No se pudo enviar tu invitacion: "+error);
			}
			catch(JSONException je){
			}
		}
		else
			throw new ServerException("No se pudo enviar tu invitacion: ");
		return result;
	}
	
	private boolean ackContactInvitation(ContactVO contact) throws ServerException{
		boolean result = false;
		String to = Constants.setPendingContactInvitationSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txIdUser", String.valueOf(MainItem.currentUser.getIdUser())));
		nameValuePairs.add(new BasicNameValuePair("txIdContact", String.valueOf(contact.getIdContact())));
		nameValuePairs.add(new BasicNameValuePair("txAcceptance", String.valueOf(contact.getStatus().equals("on")?1:0)));
		String responseString = PostUtilities.post(new PostMessage(to, nameValuePairs, this));
		if(responseString!=null && responseString.contains("success\":\"1")){
			to = Constants.getNotificationParametersSrvPath;
			nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("txIdUser", String.valueOf(contact.getIdContact())));
			responseString = PostUtilities.post(new PostMessage(to, nameValuePairs, this));
			if(responseString!=null && responseString.contains("success\":\"1")){
				try {
					JSONObject c = new JSONObject(responseString);
					String nick = c.optString("nickname");
					String token = c.optString("tokens");
					//TODO: Verify that the notification actually gets to the user
					sendContactNotification(token, Constants.NOTIFTYPE_ACCEPTANCE, nick);
					result = true;
						
				}
				catch(JSONException je){
					throw new ServerException("No se pudo enviar la aceptación de invitación.");
				}
			}
		}
		else
			throw new ServerException("No se pudo enviar la aceptación de invitación.");
		return result;
	}
	

}
