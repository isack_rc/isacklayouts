package com.item.itemmsgr.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.item.itemmsgr.models.ConversationVO;
import com.item.itemmsgr.utilities.Constants;
import com.item.itemmsgr.utilities.PostMessage;
import com.item.itemmsgr.utilities.PostUtilities;
import com.item.itemmsgr.utilities.exceptions.ServerException;

public class ConversationListController extends Controller {

	public static final int DELETE_CONVERSATION = 2;
	public static final int UPDATE_CONVERSATION = 1;
	public static final int NEW_CONVERSATION = 0;
	
	@Override
	public boolean handleMessage(int what, Object data) {
		boolean result = true;
		switch(what){
		case DELETE_CONVERSATION:
			try {
				//contact object must already contain the telephone from the txtTelephone in the activit
				deleteConversation(((Long)data));
			} catch (ServerException e) {
				e.printStackTrace();
			}
			break;
		case UPDATE_CONVERSATION:
			try {
				updateConversation(((ConversationVO)data));
			} catch (ServerException e) {
				e.printStackTrace();
			}
			break;
		case NEW_CONVERSATION:
			try {
				newConversation(((ConversationVO)data));
			} catch (ServerException e) {
				e.printStackTrace();
			}
			break;
	};
		return result;
	}

	

	private void deleteConversation(Long idConversation) throws ServerException{
		
		String to = Constants.deleteConversationSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txIdConversation", String.valueOf(idConversation)));
		
		JSONArray jsonResponse = null;
		String responseString = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if(responseString != null){
			try {
				JSONObject jsonObj = new JSONObject(responseString);
				jsonResponse = jsonObj.getJSONArray("list");
				JSONObject c = jsonResponse.getJSONObject(0);
				String success = c.getString("success");
				String error = c.getString("errMsg");
				if(success.equals("1")){
					//ConversationDAO.delete(idConversation);
					//MessageDAO.deleteByConversationId(idConversation);
					//PaticipantDAO.deleteByConversationId(idConversation);
					//MainItem.conversationList.remove(idConversation);
				}
				else
					throw new ServerException("No se pudo eliminar la conversacion: "+error);
			}
			catch(JSONException je){
			}
		}
		else
			throw new ServerException("No se pudo eliminar la conversacion");
	}
	
	private void updateConversation(ConversationVO conversation) throws ServerException{
		String to = Constants.getConversationDataSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("txIdConversation", String.valueOf(conversation.getIdConversation())));
		Long lastIdMessage = new Long(0);//= ConversationDAO.getLastIdMessage(conversation.getIdConversation());
		nameValuePairs.add(new BasicNameValuePair("txLastIdMessage",String.valueOf(lastIdMessage)));
		JSONArray jsonResponse = null;
		String responseString = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if(responseString != null){
			try {
				JSONObject jsonObj = new JSONObject(responseString);
				jsonResponse = jsonObj.getJSONArray("list");
				JSONObject c = jsonResponse.getJSONObject(0);
				String success = c.getString("success");
				String error = c.getString("error");
				if(success.equals("1")){
					JSONArray infoMessages = c.getJSONArray("messages");
					//rechargeBubbleDataMessagesServer(infoMessages);
					JSONArray infoParticipants = c.getJSONArray("participants");
                    for(int i=0; i<infoParticipants.length(); i++) {
                    	c = infoParticipants.getJSONObject(i);
                    	long idUserParticipant = c.getLong("idUser");
                    	long idConversation = c.getLong("idConversation");
                    	String nickname = c.getString("nickname");
                    	//ParticipantVO participant = ParticipantVO.findOrCreateParticipant(idUserParticipant, idConversation, nickname);
                    	//ParticipantDAO.saveOrUpdate(participant);
                    }
				}
				else
					throw new ServerException("No se pudo eliminar la conversacion: "+error);
			}
			catch(JSONException je){
			}
		}
		else
			throw new ServerException("No se pudo eliminar la conversacion");
	}
	
	private void newConversation(ConversationVO conversation) throws ServerException{
		
		String to = Constants.deleteConversationSrvPath;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		//nameValuePairs.add(new BasicNameValuePair("txIdConversation", String.valueOf(idConversation)));
		
		JSONArray jsonResponse = null;
		String responseString = PostUtilities.post(new PostMessage(to,nameValuePairs,this));
		if(responseString != null){
			try {
				JSONObject jsonObj = new JSONObject(responseString);
				jsonResponse = jsonObj.getJSONArray("list");
				JSONObject c = jsonResponse.getJSONObject(0);
				String success = c.getString("success");
				String error = c.getString("errMsg");
				if(success.equals("1")){
					//ConversationDAO.delete(conversationId);
				}
				else
					throw new ServerException("No se pudo eliminar la conversacion: "+error);
			}
			catch(JSONException je){
			}
		}
		else
			throw new ServerException("No se pudo eliminar la conversacion");
	}
}
