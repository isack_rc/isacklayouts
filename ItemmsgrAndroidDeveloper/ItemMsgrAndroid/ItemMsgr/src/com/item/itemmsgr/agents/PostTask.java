package com.item.itemmsgr.agents;

import java.io.IOException;
import java.util.PriorityQueue;
import java.util.Queue;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.xml.sax.SAXException;

import com.item.itemmsgr.controllers.Controller;
import com.item.itemmsgr.utilities.PostMessage;
import com.item.itemmsgr.utilities.PostUtilities;

/**
 * Copyright © ITEM by Tradeco 2014 All Rights Reserved. 
 * No part of this software may be reproduced without ITEM's 
 * express consent.
 * 
 * Copyright © ITEM by Tradeco 2014 Todos los derechos reservados. 
 * Ninguna porción de este trabajo de software puede ser reproducido sin el
 * consentimiento expreso de ITEM.
 * 
 * @author ITEM by Tradeco
 */
public class PostTask extends NetworkTask<Void, Void, String>{
	
	Queue<PostMessage> queue = null;
	PostMessage toSend = null;
	
	public PostTask(){
		queue = new PriorityQueue<PostMessage>();
	}
	
	public void addToQueue(PostMessage toSend){
		queue.add(toSend);
	}
	
	@Override
	protected String doNetworkAction() throws IOException, SAXException {
		PostMessage toSend = queue.peek();
		String response = PostUtilities.post(toSend);
		if(response != null){
			queue.remove(toSend);
			toSend.getOrigin().handleMessage(Controller.POST_RESPONSE, response);
		}
		return response;
	}
	

}
