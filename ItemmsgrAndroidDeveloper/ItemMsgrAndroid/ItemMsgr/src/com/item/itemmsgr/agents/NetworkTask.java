package com.item.itemmsgr.agents;


import java.io.IOException;

import org.xml.sax.SAXException;

import com.item.itemmsgr.activities.MainItem;
import com.item.itemmsgr.utilities.NetworkUtil;


import android.accounts.NetworkErrorException;
import android.os.AsyncTask;


abstract public class NetworkTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
	
	public static interface OnCompleteListener<Result> {
		public void onComplete(Result result);
	}
	
	public static interface OnSAXExceptionListener {
		public void onSAXException(SAXException exception);
	}
	
	public static interface OnIOExceptionListener {
		public void onIOException(IOException exception);
	}
	
	public static interface OnExceptionListener {
		public void onException(Exception exception);
	}
	
	public static interface OnNetworkUnavailableListener {
		public void onNetworkException(NetworkErrorException exception);
	}
	
	private Exception exception;
	private IOException ioException;
	private SAXException saxException;
	
	private boolean isComplete = false;
	public boolean isComplete() {
		return isComplete;
	}
	
	private boolean isAborted = false;
	public boolean isAborted() { return isAborted; }
	
	private OnCompleteListener<Result> completeListener;
	public void setOnCompleteListener(OnCompleteListener<Result> completeListener) {
		this.completeListener = completeListener;
	}

	private OnExceptionListener exceptionListener;
	public void setOnExceptionListener(OnExceptionListener l) {
		this.exceptionListener = l;
	}
	
	private OnExceptionListener genericExceptionListener;
	
	public void setOnGenericExceptionListener(OnExceptionListener l) {
		this.genericExceptionListener = l;
	}
	
	private OnIOExceptionListener ioExceptionListener;
	public void setOnIOExceptionListener(OnIOExceptionListener l) {
		this.ioExceptionListener = l;
	}
	
	private OnSAXExceptionListener saxExceptionListener;
	public void setOnSAXExceptionListener(SAXException l) {
		this.saxException = l;
	}
	
	private OnNetworkUnavailableListener networkUnavailableListener;
	public void setOnNetworkUnavailableListener(
			OnNetworkUnavailableListener networkUnavailableListener) {
		this.networkUnavailableListener = networkUnavailableListener;
	}
	
	
	public NetworkTask() {
		super();
	}
	
	
	@SuppressWarnings("unchecked")
	public void execute() {
		execute(null, null);
	}
	
	public void abort() {
		isAborted = true;
		cancel(true);
	}
	
	
	abstract protected Result doNetworkAction() throws IOException, SAXException;
	
	
	 protected void onPostSuccess(Result result) { }
	 protected void onPostFault(Exception e) { }
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		isComplete = false;
		isAborted = false;
		boolean hasNetworkConnection = NetworkUtil.hasInternetAccess(MainItem.getContext());
		if (!hasNetworkConnection) {
			if (networkUnavailableListener != null) {
				networkUnavailableListener.onNetworkException(new NetworkErrorException("Internet connection unavailable"));
			}
			abort();
		}
	}
	
	@Override
	protected Result doInBackground(Params... params) {
		if (isCancelled()) {
			return null;
		}
		try {
			return doNetworkAction();
		} catch (SAXException e) {
			saxException = e;
			return null;
		} catch (IOException e) {
			ioException = e;
			return null;
		} catch (Exception e) {
			exception = e;
			return null;
		}
	}
	
	@Override
	protected void onPostExecute(Result result) {
		super.onPostExecute(result);
		isComplete = true;
		if (isCancelled() || isAborted()) {
			return;
		}
		
		if (saxException != null) {
			onPostFault(saxException);
			if (saxExceptionListener != null) saxExceptionListener.onSAXException(saxException);
			if (genericExceptionListener != null) genericExceptionListener.onException(saxException);
		} else if (ioException != null) {
			onPostFault(ioException);
			if (ioExceptionListener != null) ioExceptionListener.onIOException(ioException);
			if (genericExceptionListener != null) genericExceptionListener.onException(ioException);
		} else if (exception != null) {
			onPostFault(exception);
			if (exceptionListener != null) exceptionListener.onException(exception);
			if (genericExceptionListener != null) genericExceptionListener.onException(exception);
		} 
		
		else {
			onPostSuccess(result);
			if (completeListener != null) {
				completeListener.onComplete(result);
			}
		}
	}
	
}
