package com.item.itemmsgr.daos;

import java.io.IOException;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.item.itemmsgr.models.ConversationVO;
import com.item.itemmsgr.utilities.database.DataBaseHelper;

public class ConversationDAO implements DAOInterface {

	private DataBaseHelper dbHelper;
	private SQLiteDatabase db;

	public ConversationDAO(Context ctx) {
		dbHelper = new DataBaseHelper(ctx);
		try {
			dbHelper.createDataBase();
		} catch (IOException e) {
			Log.e("Conversation Database creation Error", e.getMessage());
		}

	}

	@Override
	public Object get(long id) {
		ConversationVO newConversation = null;
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("conversation", new String[] {
				"idConversation", "status", "groupname", "badgeConversation" },
				"idConversation=?", new String[] { id + "" }, null, null, null,
				null);

		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			newConversation = new ConversationVO();
			newConversation.setIdConversation(cursor.getLong(0));
			newConversation.setStatus(cursor.getString(1));
			newConversation.setGroupname(cursor.getString(2));
			newConversation.setBadgeConversation(cursor.getString(3));
		}
		return newConversation;
	}

	@Override
	public long saveOrUpdate(Object VO) {

		ConversationVO newConversation = (ConversationVO) VO;
		ContentValues values = new ContentValues();
		values.put("status", newConversation.getStatus());
		values.put("groupname", newConversation.getGroupname());
		values.put("badgeConversation", newConversation.getBadgeConversation());
		
		db = dbHelper.getWritableDatabase();

		// UPDATE
		try {
			if (newConversation.getIdConversation() != null) {
				db.update("conversation", values, "idConversation = ?",
						new String[] { String.valueOf(newConversation.getIdConversation()) });
			} else {
				long id = db.insert("conversation", null, values);
				newConversation.setIdConversation(id);
			}
		} catch (Exception e) {
			Log.e("Conversation SaveOrUpdate Exception", e.getMessage());
		} finally {
			db.close();
		}
		return newConversation.getIdConversation();
	}

	@Override
	public int delete(long id) {
		int result = 0;
		db = dbHelper.getWritableDatabase();
		try {
			result = db.delete("conversation", " idConversation= ?",
					new String[] { String.valueOf(id) });
		} catch (Exception e) {
			Log.e("Conversation Delete Exception", e.getMessage());
		} finally {
			db.close();
		}
		return result;
	}

	@Override
	public int delete(Object VO) {
		return delete(((ConversationVO)VO).getIdConversation());
	}

	public long getLastIdMessage(Long idConversation) {
		long lastIdMessage=0L;
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("conversation", new String[] {
				"idConversation" },
				"idConversation=? order by idMessage desc limit 1", new String[] { String.valueOf(idConversation) }, null, null, null,
				null);

		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			lastIdMessage = cursor.getLong(0);
		}
		return lastIdMessage;
	}

	public ArrayList<ConversationVO> getConversationList() {
		ArrayList<ConversationVO> resultList = new ArrayList<ConversationVO>();
		
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("conversation", new String[] {
				"idConversation", "status", "groupname", "badgeConversation" },
				null, null, null, null, null,
				null);

		if (cursor != null && cursor.getCount() > 0) {
			while(cursor.moveToNext()){
				ConversationVO newConversation = new ConversationVO();
				newConversation.setIdConversation(cursor.getLong(0));
				newConversation.setStatus(cursor.getString(1));
				newConversation.setGroupname(cursor.getString(2));
				newConversation.setBadgeConversation(cursor.getString(3));
				resultList.add(newConversation);
			}
		}
		return resultList;
		
	}

}
