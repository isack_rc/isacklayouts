package com.item.itemmsgr.daos;

import java.io.IOException;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.item.itemmsgr.models.ParticipantVO;
import com.item.itemmsgr.models.UserVO;
import com.item.itemmsgr.utilities.database.DataBaseHelper;

public class ParticipantDAO implements DAOInterface {

	private DataBaseHelper dbHelper;
	private SQLiteDatabase db;

	public ParticipantDAO(Context ctx) {
		dbHelper = new DataBaseHelper(ctx);
		try {
			dbHelper.createDataBase();
		} catch (IOException e) {
			Log.e("User Database creation Error", e.getMessage());
		}
	}

	@Override
	public Object get(long id) {
		ParticipantVO newUser = null;
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("participant", new String[] { "idUser",
				"idConversation", "nickname" }, "idUser=?", new String[] { id
				+ "" }, null, null, null, null);

		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			newUser = new ParticipantVO();
			newUser.setIdUser(cursor.getLong(0));
			newUser.setIdConversation(cursor.getLong(1));
			newUser.setNickname(cursor.getString(2));
		}
		return newUser;
	}
	
	public Object get(long id, String nickname) {
		ParticipantVO newUser = null;
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("participant", new String[] { "idUser",
				"idConversation", "nickname" }, "idConversation=? and nickname=? ", new String[] { String.valueOf(id)
				,nickname }, null, null, null, null);

		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			newUser = new ParticipantVO();
			newUser.setIdUser(cursor.getLong(0));
			newUser.setIdConversation(cursor.getLong(1));
			newUser.setNickname(cursor.getString(2));
		}
		return newUser;
	}
	
	public ArrayList<ParticipantVO> getParticipantListByConversationId(int id) {
		ArrayList<ParticipantVO> participantList = new ArrayList<ParticipantVO>();
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("paticipant", new String[] { "idUser",
				"idConversation", "nickname" }, "idConversation=?", new String[] { id + "" },
				null, null, null, null);
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			do {
				ParticipantVO newParticipant = new ParticipantVO();
				newParticipant = new ParticipantVO();
				newParticipant.setIdUser(cursor.getLong(0));
				newParticipant.setIdConversation(cursor.getLong(1));
				newParticipant.setNickname(cursor.getString(2));
				participantList.add(newParticipant);
			} while (cursor.moveToNext());
		}
		return participantList;
	}

	@Override
	public long saveOrUpdate(Object VO) {

		ParticipantVO newParticipant = (ParticipantVO) VO;
		ContentValues values = new ContentValues();
		values.put("idConversation", newParticipant.getIdConversation());
		values.put("nickname", newParticipant.getNickname());
		db = dbHelper.getWritableDatabase();

		// UPDATE
		try {
			if (newParticipant.getIdUser() != 0) {
				db.update("participant", values, "idUser = ?",
						new String[] { String.valueOf(newParticipant.getIdUser()) });
			} else {
				long id = db.insert("participant", null, values);
				newParticipant.setIdUser(id);
			}
		} catch (Exception e) {
			Log.e("User SaveOrUpdate Exception", e.getMessage());
		} finally {
			db.close();
		}
		return newParticipant.getIdUser();
	}

	@Override
	public int delete(long id) {
		int result = 0;
		db = dbHelper.getWritableDatabase();
		try {
			result = db.delete("user", " idUser= ?",
					new String[] { String.valueOf(id) });
		} catch (Exception e) {
			Log.e("User Delete Exception", e.getMessage());
		} finally {
			db.close();
		}
		return result;
	}

	@Override
	public int delete(Object VO) {
		return delete(((UserVO) VO).getIdUser());
	}

	public ParticipantVO findOrCreateParticipant(long idUserParticipant,
			long idConversation, String nickname) {
		ParticipantVO part = (ParticipantVO) get(idUserParticipant);
		if(part == null){
			part = new ParticipantVO();
			part.setIdConversation(idConversation);
			part.setIdUser(idUserParticipant);
			part.setNickname(nickname);
		}
		return part;
	}
	
	public ParticipantVO findOrCreateParticipant(long idConversation, String nickname) {
		ParticipantVO part = (ParticipantVO) get(idConversation,nickname);
		if(part == null){
			part = new ParticipantVO();
			part.setIdConversation(idConversation);
			part.setNickname(nickname);
			long id = saveOrUpdate(part);
			part.setIdUser(id);
		}
		return part;
	}
}
