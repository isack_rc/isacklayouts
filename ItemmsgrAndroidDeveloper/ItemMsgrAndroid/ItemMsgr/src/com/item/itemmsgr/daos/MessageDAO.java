package com.item.itemmsgr.daos;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.item.itemmsgr.models.MessageVO;
import com.item.itemmsgr.utilities.database.DataBaseHelper;

public class MessageDAO implements DAOInterface {

	private DataBaseHelper dbHelper;
	private SQLiteDatabase db;

	public MessageDAO(Context ctx) {
		dbHelper = new DataBaseHelper(ctx);
		try {
			dbHelper.createDataBase();
		} catch (IOException e) {
			Log.e("Database creation Error", e.getMessage());
		}
	}

	@Override
	public Object get(long id) {
		MessageVO newMessage = null;
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("message", new String[] { "idMessage", "type",
				"dateSend", "idConversation", "idUser", "message",
				"dateReceived" }, "idMessage=?", new String[] { id + "" },
				null, null, null, null);

		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();

			newMessage = new MessageVO();
			newMessage.setIdMessage(cursor.getLong(0));
			newMessage.setType(cursor.getString(1));
			newMessage.setDateSent(new Timestamp(cursor.getLong(2)));
			newMessage.setIdConversation(cursor.getLong(3));
			newMessage.setIdUser(cursor.getLong(4));
			newMessage.setMessage(cursor.getString(5));
			newMessage.setDateReceived(new Timestamp(cursor.getLong(6)));
		}
		return newMessage;
	}

	public ArrayList<MessageVO> getMessageListByConversationId(long id) {
		ArrayList<MessageVO> messageList = new ArrayList<MessageVO>();
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("message", new String[] { "idMessage", "type",
				"dateSend", "idConversation", "idUser", "message",
				"dateReceived" }, "idConversation=? order by idMessage asc", new String[] { id + "" },
				null, null, null, null);
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			do {
				MessageVO newMessage = new MessageVO();
				newMessage.setIdMessage(cursor.getLong(0));
				newMessage.setType(cursor.getString(1));
				newMessage.setDateSent(new Timestamp(cursor.getLong(2)));
				newMessage.setIdConversation(cursor.getLong(3));
				newMessage.setIdUser(cursor.getLong(4));
				newMessage.setMessage(cursor.getString(5));
				newMessage.setDateReceived(new Timestamp(cursor.getLong(6)));
				messageList.add(newMessage);
			} while (cursor.moveToNext());
		}
		return messageList;
	}

	@Override
	public long saveOrUpdate(Object VO) {

		MessageVO newMessage = (MessageVO) VO;
		ContentValues values = new ContentValues();
		values.put("type", newMessage.getType());
		values.put("dateSend", newMessage.getDateSent().getTime());
		values.put("idConversation", newMessage.getIdConversation());
		values.put("idUser", newMessage.getIdUser());
		values.put("message", newMessage.getMessage());
		values.put("dateReceived", newMessage.getDateReceived().getTime());
		

		db = dbHelper.getWritableDatabase();

		// UPDATE
		try {
			if (newMessage.getIdUser() != null) {
				db.update("message",values,"idMessage = ?",
						new String[] { String.valueOf(newMessage.getIdMessage()) });
			} else {
				long id = db.insert("message", null, values);
				newMessage.setIdUser(id);
			}
		} catch (Exception e) {
			Log.e("Message SaveOrUpdate Exception", e.getMessage());
		} finally {
			db.close();
		}
		return newMessage.getIdUser();
	}

	@Override
	public int delete(long id) {
		int result = 0;
		db = dbHelper.getWritableDatabase();
		try {
			result = db.delete("message", " idMessage= ?",
					new String[] { String.valueOf(id) });
		} catch (Exception e) {
			Log.e("Message Delete Exception", e.getMessage());
		} finally {
			db.close();
		}
		return result;
	}

	@Override
	public int delete(Object VO) {
		return delete(((MessageVO) VO).getIdMessage());
	}
}
