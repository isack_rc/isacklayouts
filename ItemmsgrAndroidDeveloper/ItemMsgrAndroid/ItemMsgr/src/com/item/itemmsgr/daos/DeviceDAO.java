package com.item.itemmsgr.daos;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.Settings.Secure;
import android.util.Log;

import com.item.itemmsgr.activities.MainItem;
import com.item.itemmsgr.models.DeviceVO;
import com.item.itemmsgr.utilities.database.DataBaseHelper;

public class DeviceDAO implements DAOInterface {

	private DataBaseHelper dbHelper;
	private SQLiteDatabase db;

	public DeviceDAO(Context ctx) {
		dbHelper = new DataBaseHelper(ctx);
		try {
			dbHelper.createDataBase();
		} catch (IOException e) {
			Log.e("Device Database creation Error", e.getMessage());
		}

	}

	@Override
	public Object get(long id) {
		DeviceVO newDevice = null;
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("device", new String[] { "idDevice", "idUser",
				"identifier", "nickname", "model", "version",
				"dateBegin", "cloudId", "status" }, "idDevice=?", new String[] { id + "" }, null,
				null, null, null);

		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();

			newDevice = new DeviceVO();
			newDevice.setIdDevice(cursor.getLong(0));
			newDevice.setIdUser(cursor.getLong(1));
			newDevice.setIdentifier(cursor.getString(2));
			newDevice.setNickname(cursor.getString(3));
			newDevice.setModel(cursor.getString(4));
			newDevice.setVersion(cursor.getString(5));
			newDevice.setDateBegin(new Timestamp(cursor.getLong(6)));
			newDevice.setCloudId(cursor.getString(7));
			newDevice.setStatus(cursor.getString(8));
		}
		return newDevice;
	}
	
	public ArrayList<DeviceVO> getAllDevices(boolean loadCurrent) {
		ArrayList<DeviceVO> devices = new ArrayList<DeviceVO>();
		
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("device", new String[] { "idDevice", "idUser",
				"identifier", "nickname", "model", "version",
				"dateBegin", "cloudId", "status" }, null, null, null,
				null, null, null);

		if (cursor != null && cursor.getCount() > 0) {
			while(cursor.moveToNext()){
				DeviceVO newDevice = new DeviceVO();
				newDevice.setIdDevice(cursor.getLong(0));
				newDevice.setIdUser(cursor.getLong(1));
				newDevice.setIdentifier(cursor.getString(2));
				newDevice.setNickname(cursor.getString(3));
				newDevice.setModel(cursor.getString(4));
				newDevice.setVersion(cursor.getString(5));
				newDevice.setDateBegin(new Timestamp(cursor.getLong(6)));
				newDevice.setCloudId(cursor.getString(7));
				newDevice.setStatus(cursor.getString(8));
				if(loadCurrent && newDevice.getCloudId().equals(MainItem.regid) && newDevice.getIdentifier().equals(Secure.getString(MainItem.getContext().getContentResolver(),Secure.ANDROID_ID)))
					MainItem.currentDevice = newDevice;
			}
		}
		return devices;
	}

	@Override
	public long saveOrUpdate(Object VO) {

		DeviceVO newDevice = (DeviceVO) VO;
		ContentValues values = new ContentValues();
		values.put("idUser", newDevice.getIdDevice());
		values.put("identifier", newDevice.getIdentifier());
		values.put("nickname", newDevice.getNickname());
		values.put("model", newDevice.getModel());
		values.put("version", newDevice.getVersion());
		values.put("dateBegin", newDevice.getDateBegin().getTime());
		values.put("cloudId", newDevice.getCloudId());
		values.put("status", newDevice.getStatus());

		db = dbHelper.getWritableDatabase();
		
		// UPDATE
		try{
			if (newDevice.getIdDevice() != null) {
				db.update("device", values, "idDevice = ?", new String[] { String.valueOf(newDevice.getIdUser()) });
			} else {
				long id = db.insert("device", null, values);
				newDevice.setIdUser(id);
			}
		} catch(Exception e){
			Log.e("Database SaveOrUpdate Exception", e.getMessage());
		}
		finally{
			db.close();
		}
		return newDevice.getIdUser();
	}

	@Override
	public int delete(long id) {
		int result = 0;
		db = dbHelper.getWritableDatabase();
		try{
			result = db.delete("device", " idDevice= ?", new String[] { String.valueOf(id) });
		}catch(Exception e){
			Log.e("Database Delete Exception", e.getMessage());
		}
		finally {
			db.close();
		}
		return result;
	}

	@Override
	public int delete(Object VO) {
		return delete(((DeviceVO) VO).getIdDevice());
	}

	public boolean loadCurretDevice() {
		ArrayList<DeviceVO> result = getAllDevices(true);
		return result!=null && result.size()>0;
	}
}
