package com.item.itemmsgr.daos;

import java.io.IOException;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.item.itemmsgr.models.ContactVO;
import com.item.itemmsgr.utilities.database.DataBaseHelper;

public class ContactDAO implements DAOInterface {

	private DataBaseHelper dbHelper;
	private SQLiteDatabase db;

	public ContactDAO(Context ctx) {
		dbHelper = new DataBaseHelper(ctx);
		try {
			dbHelper.createDataBase();
		} catch (IOException e) {
			Log.e("Contact Database creation Error", e.getMessage());
		}
	}
	
	public Cursor getAllByCursor() {
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("contact", new String[] { "rowid _id", "idContact",
				"nickname", "imgContact", "telephone", "email", "fullName",
				"status", "favorite" }, null,
				null, null, null, null, null);

			return cursor;
	}

	@Override
	public Object get(long id) {
		ContactVO newContact = null;
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("contact", new String[] { "idContact",
				"nickname", "imgContact", "telephone", "email", "fullName",
				"status", "favorite" }, "idContact=?",
				new String[] { id + "" }, null, null, null, null);

		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			
			newContact = new ContactVO();
			newContact.setIdContact(id);
			newContact.setNickname(cursor.getString(1));
			newContact.setImgContact(cursor.getString(2));
			newContact.setTelephone(cursor.getString(3));
			newContact.setEmail(cursor.getString(4));
			newContact.setFullName(cursor.getString(5));
			newContact.setStatus(cursor.getString(6));
			newContact.setFavorite(cursor.getString(7));
			
		}
		return newContact;
	}
	
	public ArrayList<ContactVO> getContactList() {
		ArrayList<ContactVO> contactList = new ArrayList<ContactVO>();
		
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("contact", new String[] {  "idContact",
				"nickname", "imgContact", "telephone", "email", "fullName",
				"status", "favorite" }, null,
				null, null, null, null, null);

		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			do{
				ContactVO newContact = new ContactVO();
				newContact.setIdContact(cursor.getLong(0));
				newContact.setNickname(cursor.getString(1));
				newContact.setImgContact(cursor.getString(2));
				newContact.setTelephone(cursor.getString(3));
				newContact.setEmail(cursor.getString(4));
				newContact.setFullName(cursor.getString(5));
				newContact.setStatus(cursor.getString(6));
				newContact.setFavorite(cursor.getString(7));
				contactList.add(newContact);
			}while(cursor.moveToNext());
		}
		return contactList;
	}

	@Override
	public long saveOrUpdate(Object VO) {

		ContactVO newContact = (ContactVO) VO;
		ContentValues values = new ContentValues();
		values.put("nickname", newContact.getNickname());
		values.put("imgContact", newContact.getImgContact());
		values.put("telephone", newContact.getTelephone());
		values.put("email", newContact.getEmail());
		values.put("fullName", newContact.getFullName());
		values.put("status", newContact.getStatus());
		values.put("favorite", newContact.getFavorite());
		
		db = dbHelper.getWritableDatabase();

		// UPDATE
		try {
			if (newContact.getIdUser() != null) {
				db.update("contact", values, "idContact = ?",
						new String[] { String.valueOf(newContact.getIdContact()) });
			} else {
				long id = db.insert("contact", null, values);
				newContact.setIdUser(id);
			}
		} catch (Exception e) {
			Log.e("Contact SaveOrUpdate Exception", e.getMessage());
		} finally {
			db.close();
		}
		return newContact.getIdUser();
	}

	@Override
	public int delete(long id) {
		int result = 0;
		db = dbHelper.getWritableDatabase();
		try {
			result = db.delete("contact", " idContact= ?",
					new String[] { String.valueOf(id) });
		} catch (Exception e) {
			Log.e("Contact Delete Exception", e.getMessage());
		} finally {
			db.close();
		}
		return result;
	}

	@Override
	public int delete(Object VO) {
		return delete(((ContactVO) VO).getIdContact());
	}

	public int findContact(int idContact) {
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("contact", new String[] { "idContact" }, "idContact=?",
				new String[] { idContact + "" }, null, null, null, null);
		return cursor.getCount();
	}

	
}
