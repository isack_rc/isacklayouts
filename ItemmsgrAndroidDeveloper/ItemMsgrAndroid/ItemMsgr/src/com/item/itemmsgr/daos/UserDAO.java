package com.item.itemmsgr.daos;

import java.io.IOException;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.item.itemmsgr.activities.MainItem;
import com.item.itemmsgr.models.UserVO;
import com.item.itemmsgr.utilities.database.DataBaseHelper;

public class UserDAO implements DAOInterface {

	private DataBaseHelper dbHelper;
	private SQLiteDatabase db;

	public UserDAO(Context ctx) {
		dbHelper = new DataBaseHelper(ctx);
		try {
			dbHelper.createDataBase();
		} catch (IOException e) {
			Log.e("User Database creation Error", e.getMessage());
		}

	}

	@Override
	public UserVO get(long id) {
		UserVO newUser = null;
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("user", new String[] { "idUser", "name",
				"lastname", "telephone", "email", "imgPicture",
				"imgBackground", "nickname", "publickey", "nkey", "regkey",
				"privatekey" }, "idUser=?", new String[] { id + "" }, null,
				null, null, null);

		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();

			newUser = new UserVO();

			newUser.setIdUser(cursor.getLong(0));
			newUser.setName(cursor.getString(1));
			newUser.setLastname(cursor.getString(2));
			newUser.setTelephone(cursor.getString(3));
			newUser.setEmail(cursor.getString(4));
			newUser.setImgPicture(cursor.getString(5));
			newUser.setImgBackground(cursor.getString(6));
			newUser.setNickname(cursor.getString(7));
			newUser.setPublicKey(cursor.getString(8));
			newUser.setnKey(cursor.getString(9));
			newUser.setRegKey(cursor.getString(10));
			newUser.setPrivateKey(cursor.getString(11));
		}
		return newUser;
	}
	
	public boolean loadCurrentUser() {
		boolean result = false;
		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query("user", new String[] { "idUser", "name",
				"lastname", "telephone", "email", "imgPicture",
				"imgBackground", "nickname", "publickey", "nkey", "regkey",
				"privatekey" }, null, null, null,
				null, null, null);

		if (cursor != null && cursor.getCount() > 0) {
			
				cursor.moveToFirst();
				UserVO newUser = new UserVO();
	
				newUser.setIdUser(cursor.getLong(0));
				newUser.setName(cursor.getString(1));
				newUser.setLastname(cursor.getString(2));
				newUser.setTelephone(cursor.getString(3));
				newUser.setEmail(cursor.getString(4));
				newUser.setImgPicture(cursor.getString(5));
				newUser.setImgBackground(cursor.getString(6));
				newUser.setNickname(cursor.getString(7));
				newUser.setPublicKey(cursor.getString(8));
				newUser.setnKey(cursor.getString(9));
				newUser.setRegKey(cursor.getString(10));
				newUser.setPrivateKey(cursor.getString(11));
				MainItem.currentUser = newUser;
				result = true;
		}
		return result;
	}


	@Override
	public long saveOrUpdate(Object VO) {

		UserVO newUser = (UserVO) VO;
		ContentValues values = new ContentValues();
		values.put("name", newUser.getName());
		values.put("lastname", newUser.getLastname());
		values.put("telephone", newUser.getTelephone());
		values.put("email", newUser.getEmail());
		values.put("imgPicture", newUser.getImgPicture());
		values.put("imgBackground", newUser.getImgBackground());
		values.put("nickname", newUser.getNickname());
		values.put("publickey", newUser.getPublicKey());
		values.put("nkey", newUser.getnKey());
		values.put("regkey", newUser.getRegKey());
		values.put("privatekey", newUser.getPrivateKey());

		db = dbHelper.getWritableDatabase();
		
		// UPDATE
		try{
			if (newUser.getIdUser() != null) {
				db.update("user", values, "idUser = ?", new String[] { String.valueOf(newUser.getIdUser()) });
			} else {
				long id = db.insert("user", null, values);
				newUser.setIdUser(id);
			}
		} catch(Exception e){
			Log.e("User SaveOrUpdate Exception", e.getMessage());
		}
		finally{
			db.close();
		}
		return newUser.getIdUser();
	}

	@Override
	public int delete(long id) {
		int result = 0;
		db = dbHelper.getWritableDatabase();
		try{
			result = db.delete("user", " idUser= ?", new String[] { String.valueOf(id) });
		}catch(Exception e){
			Log.e("User Delete Exception", e.getMessage());
		}
		finally {
			db.close();
		}
		return result;
	}

	@Override
	public int delete(Object VO) {
		return delete(((UserVO) VO).getIdUser());
	}

}
